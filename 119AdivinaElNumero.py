### Define una función que pregunte al usuario un número bajo y otro alto
### y entonces genere un número aleatorio entre esos dos números y lo 
### almacene en una variable llamada "numeroAleatorio".
### Define otra función que imprima "Estoy pensando en un número"
### y entonces le pida al usuario que adivine el número que el programa
### ha generado.
### Y por último define una función que compruebe si "numeroAleatorio"
### es el mismo que el del usuario ha introducido. Si lo es, debería 
### mostrar el mensaje "Correcto", de otro modo debería mantenerse en el
### bucle indicando al usuario si el número que han introducido es demasiado
### bajo o demasiado alto y preguntandoles nuevamente por un nuevo número
### hasta que ellos adivinen el número aleatorio.


import random

def generaNumeroAleatorio():
    minimoRango = int(input("Dime un numero mínimo aleatorio: "))
    maximoRango = int(input("Dime un numero máximo aleatorio: "))
    numeroAleatorio = random.randint(minimoRango, maximoRango)
    return numeroAleatorio

def pensandoNumero():
    print("Estoy pensando en un número")
    numeroUsuario = int(input("¿Qué número estoy pensando?: "))
    return numeroUsuario

def comprobacion(numeroUsuario, numeroAleatorio):
    while(numeroUsuario != numeroAleatorio):
        if(numeroUsuario < numeroAleatorio):
            print("El numero que has dicho es muy bajo prueba otra vez")
            numeroUsuario = int(input("Inténtalo otra vez: "))
        elif(numeroUsuario > numeroAleatorio):
            print("El numero que has dicho es muy alto prueba otra vez")
            numeroUsuario = int(input("Inténtalo otra vez: "))
        print()
    print("Correcto")
    
def main():
    numeroAleatorio = generaNumeroAleatorio()
    numeroUsuario = pensandoNumero()
    comprobacion(numeroUsuario, numeroAleatorio)

main()