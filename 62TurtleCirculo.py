### Dibuja un circulo


import turtle

turtle.color("blue", "blue")
turtle.begin_fill()

radio = 100
turtle.circle(radio)

turtle.end_fill()
turtle.exitonclick()