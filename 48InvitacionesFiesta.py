### Pide por el nombre de alguien al usuario que quiere invitar a la fiesta
### después de esto muestra el mensaje "[nombre] ha sido invitado a la fiesta"
### y añade 1 al contador de invitados. Entonces pregunta si ellos quieren invitar
### a alguien más. Mantente repitiendo esto hasta queellos no quieran invitar a alguien más
### a la fiesta y entonces muestra cuanta gente ha sido invitada


totalInvitados = 0
nuevoInvitado = "s"
while nuevoInvitado.lower() == "si" or nuevoInvitado.lower() == "s": 
    nombre = input("Escribe el nombre de la persona que quieres invitar: ")
    print(nombre, "ha sido invitado a la fiesta")
    totalInvitados += 1
    nuevoInvitado = input("¿Quieres seguir invitando? (Si/No): ")
print("Hay", totalInvitados, "invitados")