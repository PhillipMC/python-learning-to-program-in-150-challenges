### Transformar una palabra a Pig Latin
### https://es.wikipedia.org/wiki/Pig_Latin

palabra = input("Dime una palabra: ")
if(palabra[0] in ('a', 'e', 'i', 'o', 'u')): # palabra[0] in "aeiou"
    print((palabra + "way").lower())
else:
    print((palabra[1:] + palabra[0] + "ay").lower())