### Usando "Libros.csv" del programa previo, pide al usuario que introduzca otro
### registro y añadelo al final del archivo. Muestra cada fila del archivo .csv
### en líneas separadas


import csv

nombreCampos = []

with open ("recursos/Libros.csv", 'r') as archivoCsv:
    nombreCampos = csv.DictReader(archivoCsv).fieldnames

with open("recursos/Libros.csv", 'a+', newline='') as archivoCsv:
    nuevoLibro = input("Dime un nuevo Libro: ")
    nuevoAutor = input("Dime el autor: ")
    nuevoAnio = input("Dime el año de publicación: ")

    escritor = csv.DictWriter(archivoCsv, fieldnames=nombreCampos)
    escritor.writerow({"Libro":nuevoLibro, "Autor":nuevoAutor, "Año Publicación":nuevoAnio})

    archivoCsv.seek(0)
    lector = csv.DictReader(archivoCsv, delimiter=',')
    for linea in lector:
        print(linea)
