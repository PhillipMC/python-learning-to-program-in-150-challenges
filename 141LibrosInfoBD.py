### Crea una nueva base de datos SQ llamada "LibrosInfo" la cual almacenará una 
### lista de autores y los libros que ellos escribieron. Tendrá 2 tablas. La primera
### debería llamarse "Autores" y contendrá (Nombre, Lugar_de_nacmiento), la segunda
### debería llamarse "Libros" y contendrá (ID, Titulo, Autor, Fecha_Publicada)


import sqlite3

def creacionBD():
    with sqlite3.connect("python-learning-to-program-in-150-challenges/recursos/LibrosInfo.db") as bd:
        cursor = bd.cursor()
        cursor.execute("""CREATE TABLE IF NOT EXISTS Autores(
            Nombre TEXT PRIMARY KEY,
            Lugar_de_Nacimiento TEXT NOT NULL);""")
            
        cursor.execute("""CREATE TABLE IF NOT EXISTS Libros(
                ID INTEGER PRIMARY KEY,
                Titulo TEXT NOT NULL,
                Autor TEXT NOT NULL,
                Fecha_Publicada DATE NOT NULL);""")
        cursor.close()
        bd.commit()
        bd.close


def insertarAutores():
    with sqlite3.connect("python-learning-to-program-in-150-challenges/recursos/LibrosInfo.db") as bd:
        cursor = bd.cursor()
        cursor.execute("""INSERT OR IGNORE INTO Autores(Nombre, Lugar_de_Nacimiento)
                    VALUES('Agatha Christie', 'Turquia'),
                        ('Cecelia Ahern', 'Dublin'),
                        ('J.K Rowling', 'Bristol'),
                        ('Oscar Wilde', 'Dublin');""")
        cursor.close()
        bd.commit()
        bd.close

def insertarLibros():
    with sqlite3.connect("python-learning-to-program-in-150-challenges/recursos/LibrosInfo.db") as bd:
        cursor = bd.cursor()
        cursor.execute("""INSERT OR IGNORE INTO Libros(ID, Titulo, Autor, Fecha_Publicada)
                    VALUES(1, 'De Profundis', 'Oscar Wilde', 1905),
                        (2, 'Harry Potter y la camara de los secretos', 'J.K. Rowling', 1998),
                        (3, 'Harry Potter y el prisionero de Azkaban', 'J.K. Rowling', 1905),
                        (4, 'Ave Lira', 'Cecelia Ahern', 2017),
                        (5, 'Asesinato en el Orient Express', 'Agatha Christie', 1934),
                        (6, 'Perfectos', 'Cecelia Ahern', 2017),
                        (7, 'The Marble Collector', 'Cecelia Ahern', 2016),
                        (8, 'Asesinato en el campo de golf', 'Agatha Christie', 1923),
                        (9, 'El retrato de Dorian Gray', 'Oscar Wilde', 1890),
                        (10, 'El misterioso señor Brown', 'Agatha Christie', 1921),
                        (11, 'El misterio de las siete esferas', 'Agatha Christie', 1929),
                        (12, 'El año en que te conocí', 'Cecelia Ahern', 2014);""")
        cursor.close()
        bd.commit()
        bd.close

if __name__ == "__main__":
    creacionBD()
    insertarAutores()
    insertarLibros()
