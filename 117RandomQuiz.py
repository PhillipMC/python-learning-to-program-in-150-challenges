### Crea un concurso simple de matemáticas el cual pedirá al usuario su nombre y
### entonces generará dos preguntas aleatorias. Almacena su nombre, las preguntas
### que les han preguntado, sus respuestas y su marcador final en un archivo .csv
### Cuando el programa se ejecute varias veces debería añadir al archivo .csv la
### información y no sobreescribir el programa


import csv
import random

preguntas = []
numeros = []
operaciones = []
respuestas = []

for indice in range(4):
    numeros.append(random.randint(1,100))
for indice in range(2):
    operaciones.append(random.choice(['+','*','-','/']))
for indice in range(2):
    preguntas.append("¿Cuánto es " + str(numeros[indice]) + str(operaciones[indice]) + str(numeros[indice+1]) + " ?")

nombre = input("Introduce tu nombre: ")
puntuacion = 0
for indice, pregunta in enumerate(preguntas): 
    respuesta = int(input(pregunta + ": "))
    if( round(eval(str(numeros[indice])+operaciones[indice]+str(numeros[indice+1])), 0) == respuesta):
        print("ok")
        puntuacion += 1
    else:
        print("no")
    respuestas.append(respuesta)

with open('recursos/ConcursoMates.csv', 'a+', newline='') as archivoCsvAdjuntar,\
     open('recursos/ConcursoMates.csv', 'r', newline='') as archivoCsvLeer:
    
    nombreCampos = ["Jugador","Preguntas","Respuestas","Puntuacion"]
    lector = csv.DictReader(archivoCsvLeer)
    escritor = csv.DictWriter(archivoCsvAdjuntar, fieldnames=nombreCampos)
    filas = list(lector)
    if(len(filas) == 0):
        escritor.writeheader()
    escritor.writerow({"Jugador": nombre, "Preguntas":preguntas, "Respuestas":respuestas, "Puntuacion":puntuacion})