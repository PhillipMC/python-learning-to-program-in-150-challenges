### Pide al usuario introducir su color favorito, si ellos introducen
### "rojo" muestra el mensaje "Me gusta el rojo también", sino 
### muestra el mensaje:
### "No me gusta el " + [color] + ", me gusta el rojo"

color = input("Dime que color te gusta: ")
if(color == "ROJO" or color == "Rojo" or color == "rojo"):
    print("Me gusta el rojo también")
else:
    print("No me gusta el " + color + ", me gusta el rojo")