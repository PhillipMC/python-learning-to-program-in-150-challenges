### Cambia el programa anterior para añadir un tercer botón que guardará la lista
### a un archivo ".csv"


import tkinter
import csv
from tkinter import messagebox

ventana = tkinter.Tk()
ventana.title("Lista Numeros")
ventana.geometry("400x400")

numero = tkinter.StringVar()
listaNumeros = tkinter.Listbox()


def introducirNumero():
    numero = texto.get()
    if(numero.isdecimal()):
        texto.delete(0, texto.index(tkinter.END))
        listaNumeros.insert(tkinter.END,numero)
        listaNumeros.pack()
    else:
        texto.delete(0, texto.index(tkinter.END))

def borrarLista():
    listaNumeros.delete(0, tkinter.END) 
    listaNumeros.pack_forget()
    texto.focus()

def guardarCsv():
    with open('python_by_example_150-scripts/recursos/ListaNumerosGui.csv', 'a+', newline='') as archivoCsvAdjuntar:
        
        nombreCampos = ["Numeros"]
        escritor = csv.DictWriter(archivoCsvAdjuntar, fieldnames=nombreCampos)
        escritor.writeheader()
        listaNumerosCsv = listaNumeros.get(0,tkinter.END)
        #diccionarioCsv = [{clave: valor for clave, valor in zip(["Numeros"], numeroDiccionario)} for numeroDiccionario in listaNumerosCsv]
        diccionarioCsv = []
        for numeroDiccionario in listaNumerosCsv:
            diccionarioCsv.append({"Numeros": numeroDiccionario})
        escritor.writerows(diccionarioCsv)
        messagebox.showinfo("Info","Lista Guardada Exitosamente")
        

texto = tkinter.Entry(ventana)
texto.focus()
 
etiqueta = tkinter.Label(ventana, text = "Escribe un numero entero")
etiqueta.config(font =("Courier", 14))

boton1 = tkinter.Button(ventana, text = "Añadir", command = introducirNumero)
boton2 = tkinter.Button(ventana, text = "Resetear", command = borrarLista)
boton3 = tkinter.Button(ventana, text = "Guardar CSV", command = guardarCsv)
 
etiqueta.pack()
texto.pack()
boton1.pack()
boton2.pack()
boton3.pack()

ventana.mainloop()