### Crea un programa que muestre una ventana con un cuadro de texto que permita
### introducir un número, un botón "Ver tabla de multiplicar" y otro botón "Borrar"
### Cuando el usuario introduzca un número en el cuadro de texto para introducir un
### número en el primer cuadro de texto y clique en el botón "Ver tabla de multiplicar"
### debería mostrar en una lista la tabla de multiplicar de ese número hasta el 12.
### Por ejemplo si el usuario introduce 100 se debería ver la lista de la siguiente
### manera: 
# 1 x 100 = 100
# 2 x 100 = 200
# 3 x 100 = 300
# 4 x 100 = 400
# 5 x 100 = 500
# 6 x 100 = 600
# 7 x 100 = 700
# 8 x 100 = 800
# 9 x 100 = 900
# 10 x 100 = 1000
# 11 x 100 = 1100
# 12 x 100 = 1200


import tkinter

ventana = tkinter.Tk()
ventana.title("Tablas Multiplicar")
ventana.geometry("475x325")
ventana.resizable(False, False)

tablaMultiplicar = tkinter.IntVar()
tablaMultiplicar.initialize(1)
pizarraMultiplicar = tkinter.Text(ventana, height=12, width=15)
entradaNumero = tkinter.Entry(ventana, width=15, textvariable=tablaMultiplicar)

def verTabla():
    pizarraMultiplicar.delete("1.0", tkinter.END)
    for i in range(1, 13):
        pizarraMultiplicar.insert(tkinter.END, f"{i} x {tablaMultiplicar.get()} = {i * tablaMultiplicar.get()}\n")

def borrarPizarra():
    entradaNumero.delete(0, tkinter.END)
    pizarraMultiplicar.delete("1.0", tkinter.END)


tkinter.Label(ventana, text = "Introduce un numero:").place(x=15, y=30)
entradaNumero.place(x=165, y=30)
tkinter.Button(ventana, text="Ver Tabla Multiplicar", command = verTabla).place(x=300, y=30)
tkinter.Button(ventana, text="Borrar", command = borrarPizarra).place(x=300, y=70)
pizarraMultiplicar.place(x=165, y=70)

ventana.mainloop()