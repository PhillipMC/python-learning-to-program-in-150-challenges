### Crea un programa que pida al usuario introducir un nombre y entonces el género
### para esa persona de una lista desplegable. Debería entonces añadir el nombre y
### el género (separado por una coma) a una lista cuando el usuario clique en un 
### botón.


import tkinter


ventana = tkinter.Tk()
ventana.title("Nombre y Género")
ventana.geometry("400x400")


genero = tkinter.StringVar(ventana)
genero.set("Hombre")


def insertarNombre():
    listaNombres.insert(tkinter.END, f"{entradaNombre.get()}, {genero.get()}")

etiqueta = tkinter.Label(text = "Introduce tu Nombre y Género")
listaGeneros = tkinter.OptionMenu(ventana, genero, "Hombre", "Mujer")
entradaNombre = tkinter.Entry(ventana, name = "nombre")
barraNavegacion = tkinter.Scrollbar(orient=tkinter.VERTICAL)

listaNombres = tkinter.Listbox(
    ventana,
    yscrollcommand = barraNavegacion.set,
    font=("Helvetica", 12)
)

barraNavegacion.config(command=listaNombres.yview)


etiqueta.grid(row = 0, column = 1, pady = 20)
listaGeneros.grid(row = 1, column = 0, padx = 20, pady = 20)
entradaNombre.grid(row = 1, column = 1)
tkinter.Button(text= "\u2713", command = insertarNombre).grid(row = 1, column = 2)
listaNombres.grid(row = 2, column = 1)
barraNavegacion.grid(row = 2,column=2)


ventana.mainloop()