### Guarda varías imagenes en la misma carpeta como tu programa y llámalos
### 1.gif, 2.gif, 3.gif... etc. Todos tienen que ser .gif. Muestra en una
### ventana y pide al usuario que introduzca un número. Debería entonces usar
### ese número para escoger el nombre de archivo correcto y mostrar la imagen
### correcta. 


import tkinter
from PIL import Image as Imagen, ImageTk as ImagenTkinter


ventana = tkinter.Tk()
ventana.title("Elige un numero")
ventana.geometry("400x400")

marcoFoto = tkinter.Label(ventana, width = 200, height = 200)
numeroUsuario = tkinter.Entry()

def numeroGif():
    numero = numeroUsuario.get()
    if(int(numero) in range(1, 4)):
        # foto = tkinter.PhotoImage(file = "python-learning-to-program-in-150-challenges/recursos/" + numero + ".gif")
        
        ### PARA REDIMENSIONAR LAS FOTOS A UNA TUPLA (width, height) ESPECIFICO // ELIMINAR PARA USAR tkinter NO PIL
        fotoPIL = Imagen.open("python-learning-to-program-in-150-challenges/recursos/" + numero + ".gif")
        imagenRedimensionada = fotoPIL.resize((200, 200))
        foto = ImagenTkinter.PhotoImage(imagenRedimensionada)

        marcoFoto.configure(image = foto)
        marcoFoto.image = foto
        marcoFoto.update()
    else:
        print("Elige un numero entre el 1-3")




tkinter.Label(ventana, text = "Elige un numero 1-3").pack(pady = 10)
numeroUsuario.pack()
tkinter.Button(text = "introduce numero", command = numeroGif).pack()
marcoFoto.pack()



ventana.mainloop()