### Dibuja una estrella de 5 puntas


import turtle
import random

colores = ["black", "indianred", "ForestGreen", "firebrick" ]
turtle.color(random.choice(colores))
turtle.begin_fill()


for i in range(0,5):
    turtle.forward(100)
    turtle.right(144)
turtle.end_fill()


turtle.exitonclick()