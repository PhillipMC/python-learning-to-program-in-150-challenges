### Crea un nuevo programa que genere dos números aleatorios enteros entre 10-50
### El programa debería preguntar al usuario para que sume los números y escriba
### la respuesta. Si ellos dan con la respuesta correcta, muestra una imagen con 
### un tick, si ellos dan una respuesta equivocada, muestra otra imagen con una 
### cruz. Deberían poder clicar en el botón siguiente para obtener otra respuesta


import tkinter
import random


ventana = tkinter.Tk()
ventana.title("Sumar dos números")
ventana.geometry("400x400")
ventana.config( bg = "light blue")

etiquetaPregunta = tkinter.Label()
entradaPregunta = tkinter.Entry()
botonRespuesta = tkinter.Button()
numero1 = 0
numero2 = 0


def pregunta():
    global etiquetaPregunta, entradaPregunta, botonRespuesta
    global numero1, numero2
    if(etiquetaPregunta):
        etiquetaPregunta.destroy()
        entradaPregunta.destroy()
    numero1 = random.randint(10, 50)
    numero2 = random.randint(10, 50)
    etiquetaPregunta = tkinter.Label(ventana, text = "¿Cuánto es " + str(numero1) + '+' + str(numero2) + '?')
    entradaPregunta = tkinter.Entry(ventana, name = "pregunta", width = 3)

    botonRespuesta = tkinter.Button(ventana, text = "Respuesta", command = respuesta)
    botonRespuesta.pack(pady = 20)

    etiquetaPregunta.place(rely = 0.5, x = 40, relx = 0.3)
    entradaPregunta.place(rely = 0.5, x = 180, relx = 0.3)
    botonRespuesta.place(rely = 0.6, x = 40, relx = 0.4)


def respuesta():
    global numero1, numero2
    pregunta = entradaPregunta.get()
    etiquetaPregunta.destroy()
    entradaPregunta.destroy()
    botonRespuesta.destroy()
    nuevaVentana = tkinter.Toplevel(ventana)
    nuevaVentana.geometry("200x200")
    nuevaVentana.title("Respuesta")
    etiquetaRespuesta = ''
    if(str(numero1 + numero2) == pregunta):
        foto = tkinter.PhotoImage(file = "python-learning-to-program-in-150-challenges/recursos/check.gif")
        etiquetaRespuesta = "CORRECTO"
        # REDUCE EL TAMAÑO 4 VECES DE LA IMAGEN ORIGINAL
        foto = foto.subsample(4, 4)
    else:
        foto = tkinter.PhotoImage(file = "python-learning-to-program-in-150-challenges/recursos/error.gif")
        etiquetaRespuesta = "ERROR"
    cajaFoto = tkinter.Label(nuevaVentana, text = etiquetaRespuesta, compound='center', image = foto, font=('Arial',20,'bold','underline'))
    cajaFoto.image = foto
    cajaFoto.pack()


botonPregunta = tkinter.Button(ventana, text = "Pregunta", command = pregunta)
botonPregunta.pack(pady = 20)

ventana.mainloop()