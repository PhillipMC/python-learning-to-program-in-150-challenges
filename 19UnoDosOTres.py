### Pide al usuario introducir "1" "2" ó "3" si introducen "1"
### muestra el mensaje "Gracias" si introducen "2" muestra "bien hecho"
### Si introducen "3" muestra "Correcto" si ellos introducen otra cosa muestra "Error"

numero = int(input("Elige entre 1, 2 ó 3: "))
if(numero == 1):
    print("Gracias")
elif(numero == 2):
    print("Bien Hecho")
elif(numero == 3):
    print("Correcto")
else:
    print("Error")