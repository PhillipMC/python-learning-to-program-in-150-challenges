### Escribe un programa que puede ser usado en vez de tirar un dado de 6
### lados en un juego de mesa. Cuando el usuario clique un botón debería
### mostrar un número aleatorio entero entre 1-6.


import tkinter
import random

ventana = tkinter.Tk()
ventana.title("Dado")
ventana.geometry("400x400")

def lanzaDado():

    mensaje.destroy
    mensaje["text"] = random.randint(1,6)
    
    # Colores: http://cs111.wellesley.edu/~cs111/archive/cs111_spring15/public_html/labs/lab12/colors.png
    mensaje["bg"] = "goldenrod"
    mensaje["fg"] = "purple4"
    mensaje["justify"] = "center"
    mensaje["relief"] = "sunken"
    mensaje.place(relx=0.5, rely=0.3, anchor="center")


mensaje = tkinter.Message(ventana)
boton = tkinter.Button(ventana, text = 'Lanzar Dado', command = lanzaDado)

boton.pack(fill = "x", expand = True)


ventana.mainloop()