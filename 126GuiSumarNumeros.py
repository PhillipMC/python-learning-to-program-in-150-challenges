### Crea un programa que pida al usuario que introduzca un número en una caja
### Cuando se clique en un botón añadirá ese número a un total y se mostrará en
### otra caja de texto. Esto puede realizarse tantas veces como se quiera, si el
### el usuario quiere mantenerse sumando números al total. 
### Debería haber otro botón que reinicie el total a 0 y vacíe el cuadro de texto
### original.


import tkinter

ventana = tkinter.Tk()
ventana.title("Sumar Numeros")
ventana.geometry("400x400")

variableCredito = 0

def sumar():
    mensaje.destroy
    numero = int(entradaNombre.get())
    global variableCredito 
    variableCredito += numero
    entradaNombre.delete(0, "end")
    # Colores: http://cs111.wellesley.edu/~cs111/archive/cs111_spring15/public_html/labs/lab12/colors.png
    mensaje["bg"] = "blue4"
    mensaje["fg"] = "PaleTurquoise2"
    mensaje["justify"] = "center"
    mensaje["relief"] = "sunken"
    mensaje["text"] = variableCredito
    mensaje.place(relx=0.5, rely=0.3, anchor="center")

def reseteoBoton():
    mensaje.destroy
    global variableCredito
    variableCredito = 0
    entradaNombre.delete(0, "end")
    mensaje["text"] = variableCredito
    mensaje.place(relx = 0.5, rely = 0.3, anchor = "center")


nombreEtiqueta = tkinter.Label(ventana, text = 'Sumar', font=('calibre', 10, 'bold'))
entradaNombre = tkinter.Entry(ventana, textvariable = variableCredito, font=('calibre',10,'normal'))
mensaje = tkinter.Message(ventana)
boton1 = tkinter.Button(ventana, text = 'Sumar', command = sumar)
boton2 = tkinter.Button(ventana, text = 'Reseteo', command = reseteoBoton)

boton1.pack(side="left")
boton2.pack(side="right")

nombreEtiqueta.place(relx = 0.3, rely = 0.2, anchor = "center")
entradaNombre.place(relx = 0.6, rely = 0.2, anchor = "center")

ventana.mainloop()