### Importa la información del archivo "Libros.csv" a una lista. Muestra la lista
### al usuario pideles que seleccionen cual fila de la lista ellos quieren eliminar
### y eliminala de la lista. Pide al usuario cual información ellos quieren cambiar 
### y permiteles cambiarla. Escribe la información resultante al archivo .csv original
### sobreescribiendo la información existente con la información modificada


import csv

listaPeliculas = []
nombreCampos = []

with open ("recursos/Libros.csv", 'r') as archivoCsv:
    nombreCampos = csv.DictReader(archivoCsv).fieldnames
    archivoCsv.seek(0)
    lector = csv.DictReader(archivoCsv)
    for linea in lector:
        listaPeliculas.append(linea)

for indice, linea in enumerate(listaPeliculas):
    print(str(indice) + ')',linea)
print('\n')

lineaBorrar = int(input("¿Qué linea quieres eliminar?: "))
del listaPeliculas[lineaBorrar]
print('\n')

for indice, linea in enumerate(listaPeliculas):
    print(str(indice) + ')',linea)
print('\n')

lineaCambiar = int(input("¿Qué línea quieres cambiar?: "))
listaPeliculas[lineaCambiar]["Libro"] = input("¿A qué libro quieres actualizar?: ")
listaPeliculas[lineaCambiar]["Autor"] = input("¿A qué autor quieres actualizar?: ")
listaPeliculas[lineaCambiar]["Año Publicación"] = input("¿A qué año quieres actualizar?: ")

with open('recursos/Libros.csv', 'w', newline='') as archivoCsv:
    
    escritor = csv.DictWriter(archivoCsv, fieldnames=nombreCampos)

    escritor.writeheader()
    escritor.writerows(listaPeliculas)