### Usando la BD "GuiaTelefonos.db" escribe un programa que muestre el siguiente menú
# "1: Ver listado telefónico"
# "2: Añadir al listado telefónico"
# "3: Buscar por primer Apellido"
# "4: Borrar persona del listado"
# "5: Salir
### Si se selecciona "1" se debería ver la guía telefónica completa. Si se selecciona
### "2" se debería permitir añadir al listado telefónico una nueva persona. Si se selecciona
### "3" se debería preguntar por un Apellido y entonces mostrar solo los registros de las personas
### con el mismo apellido. Si ellos seleccionan "4", se debería pedir un ID y entonces borrar 
### ese registro de la tabla. Si ellos seleccionan "5", debería finalizar el programa. Finalmente
### debería mostrarr un mensaje de error si se introduce una seleccion incorrecta del menú.
### El menú debería aparecer siempre hasta que se seleccione la opción "5".


# https://www.codigopiton.com/como-hacer-un-menu-de-usuario-en-python/

import sqlite3
import os

def mostrar_menu(opciones):
    print('Seleccione una opción:')
    for clave in sorted(opciones):
        print(f' {clave}) {opciones[clave][0]}')


def leer_opcion(opciones):
    while (a := input('Opción: ')) not in opciones:
        print('Opción incorrecta, vuelva a intentarlo.')
    return a


def ejecutar_opcion(opcion, opciones):
    opciones[opcion][1]()


def generar_menu(opciones, opcion_salida):
    opcion = None
    while opcion != opcion_salida:
        mostrar_menu(opciones)
        opcion = leer_opcion(opciones)
        ejecutar_opcion(opcion, opciones)
        print()


def menu_principal():
    opciones = {
        '1': ('Ver listado telefónico', accion1),
        '2': ('Añadir al listado telefónico', accion2),
        '3': ('Buscar por primer Apellido', accion3),
        '4': ('Borrar persona del listado', accion4),
        '5': ('Salir', salir)
    }

    generar_menu(opciones, '5')


def accion1():
    with sqlite3.connect("python_by_example_150-scripts/recursos/GuiaTelefonos.db") as db:
        cursor = db.cursor()
        cursor.execute("SELECT * FROM nombres")
        elementos = ("ID", "Nombre", "Apellido", "Numero Telefónico")
        for nombre in cursor.fetchall():
            print(f"{elementos[0]} = {nombre[0]}\n"
                  f"{elementos[1]} = {nombre[1]}\n"
                  f"{elementos[2]} = {nombre[2]}\n"
                  f"{elementos[3]} = {nombre[3]}\n")
        db.close

def accion2():
    with sqlite3.connect("python_by_example_150-scripts/recursos/GuiaTelefonos.db") as db:
        cursor = db.cursor()
        nuevoID = input("Introduce un ID para el nuevo registro: ")
        nuevoNombre = input("Introduce un Nombre para el nuevo registro: ")
        nuevoApellido = input("Introduce un Apellido para el nuevo registro: ")
        nuevoTelefono = input("Introduce un Telefono para el nuevo registro: ")
        cursor.execute("""INSERT INTO nombres(ID, Nombre, Apellido, Telefono)
                        VALUES(?,?,?,?)""", (nuevoID, nuevoNombre, nuevoApellido, nuevoTelefono))
        db.commit
        db.close

def accion3():
    with sqlite3.connect("python_by_example_150-scripts/recursos/GuiaTelefonos.db") as db:
        cursor = db.cursor()
        apellidoBuscar = input("Introduce el apellido a buscar: ")
        # https://stackoverflow.com/questions/16856647/sqlite3-programmingerror-incorrect-number-of-bindings-supplied-the-current-sta
        cursor.execute("""SELECT * FROM nombres WHERE Apellido=?""", [apellidoBuscar])
        for nombre in cursor.fetchall():
            print(f"{nombre[0]}\n"
                  f"{nombre[1]}\n"
                  f"{nombre[2]}\n"
                  f"{nombre[3]}\n")
        db.close

def accion4():
    with sqlite3.connect("python_by_example_150-scripts/recursos/GuiaTelefonos.db") as db:
        cursor = db.cursor()
        idEliminar = input("Introduce el ID a eliminar: ")
        cursor.execute("""DELETE FROM nombres WHERE ID=?""", [idEliminar])
        for nombre in cursor.fetchall():
            print(f"{nombre[0]}\n"
                  f"{nombre[1]}\n"
                  f"{nombre[2]}\n"
                  f"{nombre[3]}\n")
        db.close

def salir():
    # https://stackoverflow.com/questions/73663/how-do-i-terminate-a-script
    os._exit(0)

if __name__ == '__main__':
    menu_principal()