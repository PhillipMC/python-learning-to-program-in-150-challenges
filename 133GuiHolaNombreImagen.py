### Crea tu propio incono que consiste en varias líneas verticales multicolores.
### crea un logo que mida 200x150. Crea un programa con un cuadro de texto para
### introducir su nombre y otro cuadro de texto debajo que ponga "Presioname"
### y debería mostrar "Hola [nombre_introducido]" en el segundo cuadro de texto,
### al lado del botón "Presioname"


import tkinter


def mostrarNombre():

    mensaje.grid_forget()
    nombre = entradaNombre.get()
    mensaje["text"] = "Hola " +  nombre
    entradaNombre.delete(0, "end")
    mensaje.grid(row=2, column=1)


ventana = tkinter.Tk()
ventana.title("Imagen e Icono")
ventana.geometry("400x400")
ventana.config( bg = "light blue")

# PARA AÑADIR UN ICONO EN LINUX, FORMATO 'xbm' ('ico' EN WINDOWS) Y AL 
# AÑADIR EL path PONER '@' AL INICIO DE LA RUTA
#ventana.iconbitmap('@python-learning-to-program-in-150-challenges/square-64.xbm')


# CON ESTAS DOS LINEAS AÑADIMOS EL ICONO PNG
icono = tkinter.PhotoImage(file = "python-learning-to-program-in-150-challenges/recursos/133moonArt.png")
ventana.tk.call('wm', 'iconphoto', ventana._w, icono)

foto = tkinter.PhotoImage(file = "python-learning-to-program-in-150-challenges/recursos/133moonArt.png")
cajaFoto = tkinter.Label(ventana, image = foto)
cajaFoto.image = foto


mostrarNombre = tkinter.StringVar()




etiquetaNombre = tkinter.Label(ventana, text = "Introduce tu nombre")
entradaNombre = tkinter.Entry(ventana, name = "nombre")


mensaje = tkinter.Message(ventana)
boton = tkinter.Button(ventana, text = "Presioname", command = mostrarNombre)


cajaFoto.grid(row = 0, column= 1)
etiquetaNombre.grid(row = 1, column = 0, padx = 10, pady = 20)
entradaNombre.grid(row = 1, column = 1)
boton.grid(row=2, column=0)


ventana.mainloop()