### Realiza el cifrado césar en un programa que encripte y desencripte mensajes
###  de la siguiente manera primero debería mostrar un menú que muestre lo siguiente:
# "1) Codifica un texto"
# "2) Decodifica un texto"
# "3) Salir"
# 
# "Introduce tu selección:"
### Si el usuario introduce "1", debería ser capaz de introducir un mensaje 
### (incluyendo los espacios) y entonces introducir un número. El programa debería
### codificar el mensaje cifrandolo con el número de letras espaciadas según el número
### una vez haya sido aplicada.
### Si el usuario selecciona el "2", ellos deberían introducir un mensaje a descodificar
### y el número de saltos que realizará el cifrado para descodificarlo.
### Si se selecciona el "3" debería detenerse el programa.
### Si se elige "1" o "2" debería después de hacer la acción correspondiente volver a 
### salir el menú hasta que se seleccione el "3"


import string

def calculoIndice(indiceLetra, numeroCodificar, longitudLetras):
    indiceCalculado = indiceLetra + numeroCodificar
    while abs(indiceCalculado) > longitudLetras+1:
        indiceCalculado %= (longitudLetras)
    if(indiceCalculado > longitudLetras -1):
        indiceFinal = indiceCalculado    
    elif(indiceCalculado < 0):
        if(indiceCalculado > 0):
            indiceCalculado *= -1
        indiceFinal = longitudLetras + indiceCalculado
        indiceCalculado = indiceFinal

    return indiceCalculado



def cifradoCesar(mensaje, numeroCodificar):
    mensajeCifradoResultante = ""
    letrasMinusculas = list(string.ascii_lowercase)
    letrasMayusculas =  list(string.ascii_uppercase)
    # letrasMinusculas.insert(14, 'ñ') 
    # letrasMayusculas.insert(14, 'Ñ')
    print(letrasMinusculas)
    for letra in mensaje:
        if(letra in letrasMayusculas):
            indiceLetra = letrasMayusculas.index(letra)
            indiceLetraCalculado = calculoIndice(indiceLetra, numeroCodificar, len(letrasMayusculas))
            mensajeCifradoResultante += letrasMayusculas[indiceLetraCalculado]
        elif(letra in letrasMinusculas):
            indiceLetra = letrasMinusculas.index(letra)
            indiceLetraCalculado = calculoIndice(indiceLetra, numeroCodificar, len(letrasMinusculas))
            mensajeCifradoResultante += letrasMinusculas[indiceLetraCalculado]
        else:
            mensajeCifradoResultante += letra
    return mensajeCifradoResultante


def codificar():
    mensaje = input("Introduce tu mensaje: ")
    numero = input("Introduce un numero para codificar el mensaje: ")
    mensajeCifradoResultante = cifradoCesar(mensaje, int(numero))
    print(mensajeCifradoResultante)

def decodificar():
    mensaje = input("Introduce tu mensaje: ")
    numero = input("Introduce un numero para codificar el mensaje: ")
    mensajeCifradoResultante = cifradoCesar(mensaje, int(numero)* -1)
    print(mensajeCifradoResultante)

def menu():
    seleccion = ""
    while( seleccion != "3"):
        print("1) Codifica un texto")
        print("2) Decodifica un texto")
        print("3) Salir")
        print()
        seleccion = input("Introduce tu selección: ")
        match seleccion:
            case "1":
                codificar()
            case "2":
                decodificar()
            case "3":
                print("Adiós")
            case _:
                print("Elige entre 1, 2 ó 3 a la hora de introducir un número")


if __name__ == "__main__":
    menu()