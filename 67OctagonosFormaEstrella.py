### Crea un patrón en forma de estrella de 10 puntas con decagonos


import turtle
import random

colores = ["black", "indianred", "ForestGreen", "firebrick", "pink", "aquamarine", "Deeppink3" ]
turtle.color(random.choice(colores))

turtle.speed("fast")

turtle.penup()
turtle.forward(100)
turtle.pendown()
turtle.pensize(3)

for i in range(0,10):
    turtle.right(36)
    for i in range(0,8):
        
        turtle.right(45)
        turtle.forward(100)

turtle.exitonclick()