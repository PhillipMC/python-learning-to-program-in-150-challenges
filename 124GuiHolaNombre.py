### Crea una ventana que pregunte al usuario para que introduzca su nombre.
### Cuando ellos cliquen en un botón debería mostrar el mensaje "Hola [nombre_usuario]"
### y cambiar el color de fondo y el color del texto en la caja del mensaje


import tkinter

ventana = tkinter.Tk()
ventana.title("Nombre")
ventana.geometry("400x400")

variableNombre = ""

def envio():

    mensaje.destroy
    nombre = entradaNombre.get()
    mensaje["text"] = "Hola " +  nombre
    entradaNombre.delete(0, "end")
    # Colores: http://cs111.wellesley.edu/~cs111/archive/cs111_spring15/public_html/labs/lab12/colors.png
    mensaje["bg"] = "goldenrod"
    mensaje["fg"] = "purple4"
    mensaje["justify"] = "center"
    # podemos usar "pack()" "grid()"
    mensaje.place(relx=0.5, rely=0.5, anchor="center")


nombreEtiqueta = tkinter.Label(ventana, text = 'Nombre', font=('calibre', 10, 'bold'))
entradaNombre = tkinter.Entry(ventana, textvariable = variableNombre, font=('calibre',10,'normal'))
mensaje = tkinter.Message(ventana)
boton = tkinter.Button(ventana, text = 'Envío', command = envio)

nombreEtiqueta.grid(row=0,column=0)
entradaNombre.grid(row=0,column=1)
boton.grid(row=0,column=2)

ventana.mainloop()
