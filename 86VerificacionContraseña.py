### Pide al usuario que introduzca una contraseña. Pide que la introduzca de nuevo
### Si las dos contraseñas coinciden, muestra "Gracias", si las contraseñas coinciden
### pero no coinciden las letras minúsculas y mayúsculas muestra un mensaje de error 
### consecuente sino coinciden en absoluto muestra "Las contraseñas no coinciden"


password = input("Introduce una contraseña: ")
passwordVerificacion = input("Repite la contraseña: ")
if(password == passwordVerificacion):
    print("Gracias")
else:
    if(password.lower() == passwordVerificacion.lower()):
        print("Reescribela fijandote que mayusculas y minusculas cuentan a la hora de verificar si está bien escrita")
    else:
        print("Las contraseñas no coinciden")