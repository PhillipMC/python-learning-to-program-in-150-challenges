### Pide al usuario introducir un numero entre 10 y 20 (inclusive)
### si ellos introducen un numero en este rango, muestra el mensaje
### "Gracias", de otro modo muestra el mensaje, "Respuesta incorrecta"

numero = int(input("Dame un numero entre 10 y 20: "))
if(10 <= numero <= 20):
    print("Gracias")
else:
    print("Incorrecto")