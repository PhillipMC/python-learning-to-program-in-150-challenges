### Usando la canción "10 botellas verdes" muestra las lineas correspondientes a la canción
### y luego preguntandole al usuario cuantas botellas verdes hay si su respuesta no es la correcta
### en ese momento preguntale al usuario "¿Cuantas botellas verdes habría colgando en la pared?: "
### hasta que diga la cifra correcta de botellas y sigue la canción, cuando llegue a 0 botellas verdes
### muestra el mensaje "No hay más botellas verdes colgando en la pared


botellas = 10
while botellas > 0:
    print(botellas, "botellas verdes")
    print("Colgando en la pared")
    print("Si una botella")
    print("Se quisiera caer")
    botellasColgando = int(input("¿Cuantas botellas verdes habría colgando en la pared?: "))
    while botellasColgando != botellas-1:
        print("No, prueba otra vez")
        botellasColgando = int(input("¿Cuantas botellas verdes habría colgando en la pared?: "))
    botellas -= 1
print("No hay más botellas verdes colgando en la pared")