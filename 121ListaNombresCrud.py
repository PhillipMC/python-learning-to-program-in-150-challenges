### Crea un programa que permita al usuario trabajar con una lista de nombres.
### Deberías mostrar un menú que permita añadir a la lista, cambiar un nombre
### en la lista, borrar un nombre de la lista o ver todos los nombres de la 
### lista. También debería tener una opción paraa finalizar el programa.
### Si se selecciona una opción que no es relevante, mostrar un mensaje de error
### Después que se elija una opción y se realice su acción correspondiente, se
### debería ver el menú de nuevo sin reiniciar el programa. Debe ser lo más sencillo
### posible.


listaNombres = []

def menu():
    while(True):
        print("1) Añadir un nombre")
        print("2) Cambiar un nombre")
        print("3) Borrar un nombre")
        print("4) Ver la lista de nombres")
        print("0) Salir")
        seleccion = input("Elige un opción: ")
        match seleccion:
            case "1":
                nuevoNombre()
            case "2":
                cambiarNombre()
            case "3":
                borrarNombre()
            case "4":
                verLista()
            case "0":
                break
            case _:
                print("Elige una opción válida (0-4): ")

def nuevoNombre():
    listaNombres.append(input("Nuevo nombre a añadir: "))

def cambiarNombre():
    nombreCambiar = input("Dime el nombre que quieras cambiar: ")
    indiceNombreCambio = listaNombres.index(nombreCambiar)
    listaNombres[indiceNombreCambio] = input("Dime el nuevo nombre que quieras: ")

def borrarNombre():
    nombreBorrar = input("Dime el nombre que quieras borrar: ")
    del listaNombres[listaNombres.index(nombreBorrar)]

def verLista():
    print()
    for nombre in listaNombres:
        print(nombre)
    print()

def main():
    menu()

main()