### Aleatoriamente escoge un número entre 1 y 5. Pide al usuario que elija
### si el usuario escoge el mismo numero que el valor aleatorio, muestra el mensaje
### "Bien Hecho" sino pidele que escojan un segundo número. Si ellos adivinan la segunda vez
### muestra el mensaje "Correcto" sino muestra "Perdiste, más suerte la próxima vez"


import random

numero = random.randint(1,5)
usuarioNumero = int(input("Escoge un numero entre el 1-5: "))
if(numero == usuarioNumero):
    print("Bien Hecho")
else:
    if(usuarioNumero > numero):
        print("Numero elegido muy alto")
    else:
        print("Numero elegido muy bajo")
    usuarioNumero = int(input("Escoge otro numero entre el 1-5: "))
    if(numero == usuarioNumero):
        print("Correcto")
    else:
        print("Perdiste, más suerte la próxima vez")