### Establece total=0 mientras e total sea 50 o inferior pide al usuario que introduzca
### un número. Suma ese número al total e imprime el mensaje "El total es [total]"
### para el programa cuando "total" sea mayor que 50 

total = 0
while total <= 50:
    numero = int(input("Numero a añadir al total: "))
    total += numero
print("El total es es", total)