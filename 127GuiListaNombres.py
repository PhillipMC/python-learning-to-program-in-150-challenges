### Crea una ventana que pregunte al usuario que introduzca un nombre en una caja 
### de texto. Cuando ellos cliquen en un botón añadirá al final de la lista que 
### es mostrada en pantalla. Crea otro botón el cual borrará la lista.


import tkinter

ventana = tkinter.Tk()
ventana.title("Lista Nombres")
ventana.geometry("400x400")

nombre = tkinter.StringVar()
listaNombres = tkinter.Listbox()


def introducirNombre():
    nombre = texto.get()
    texto.delete(0, texto.index(tkinter.END))
    listaNombres.insert(tkinter.END,nombre)
    listaNombres.pack()

def borrarLista():
    listaNombres.delete(0, tkinter.END) 
    listaNombres.pack_forget()
    texto.focus()

 
texto = tkinter.Entry(ventana)
texto.focus()
 
etiqueta = tkinter.Label(ventana, text = "Escribe tu nombre")
etiqueta.config(font =("Courier", 14))

boton1 = tkinter.Button(ventana, text = "Añadir", command = introducirNombre)

boton2 = tkinter.Button(ventana, text = "Resetear", command = borrarLista)
 
etiqueta.pack()
texto.pack()
boton1.pack()
boton2.pack()

ventana.mainloop()