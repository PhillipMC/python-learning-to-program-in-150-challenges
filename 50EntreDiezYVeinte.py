### Pide al usuario que introduzca un número entre 10 y 20. Si ellos introducen un valor
### por debajo de 10, muestra el mensaje "Valor muy bajo" si el valor está por encima de
### 20 muestra el mensaje "Valor muy alto", manten repitiendo esto hasta que introduzcan un
### valor entre 10 y 20 y cuando lo hagan muestra el mensaje "Gracias"

numero = 0
numero = int(input("Adivina el numero: "))
while 10 < numero < 20:
    if(numero < 10):
        print("Valor muy bajo")
    else:
        print("Valor muy alto")
    numero = int(input("Adivina el numero: "))
print("Gracias")