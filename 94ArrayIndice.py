### Muestra un array de 5 números. Pide al usuario que seleccione uno de los números
### Una vez ellos han seleccionado un número, muestra la posición de ese elemento en el
### array, si ellos introducen algo que no está en el array, pídeles que lo intenten
### nuevamente hasta que pongan un valor que esté en el array

from array import *

numeros = array('i',[100,110,114,115,116])
print(numeros)
numeroSeleccionado = int(input("Selecciona un numero de los numeros de la lista: "))
while not numeroSeleccionado in numeros:
    numeroSeleccionado = int(input("Selecciona un numero de los numeros de la lista: "))
print("El numero", str(numeroSeleccionado), "está en la posición", str(numeros.index(numeroSeleccionado)))