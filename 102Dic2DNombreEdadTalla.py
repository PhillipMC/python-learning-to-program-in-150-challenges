### Pide al usuario que introduzca el nombre, la edad y la talla de pie, para
### 4 personas. Pide una persona recien introducida y muestra su talla de pie y 
### edad.


diccionarioPersonas = {}
for i in range(4):
    nombre = input("Escribe un nombre: ").title()
    edad = input("Escribe la edad: ")
    tallaZapato = round(float(input("Escribe la talla de zapato: ")), 1)
    diccionarioPersonas.update({nombre:{"Edad":edad, "Talla":tallaZapato}})
nombreMostrar = input(f"Escribe un nombre que quieras visualizar de los disponibles {list(diccionarioPersonas.keys())}: ").title()
print(diccionarioPersonas[nombreMostrar]) if nombreMostrar in list(diccionarioPersonas.keys()) else None