### Pregunta al usuario su edad, si ellos dicen "18" o más
### imprime "Puedes ir a votar" si ellos tienen "17"
### "Puedes aprender a conducir" si ponen "16" imprime
### "Puedes comprar un décimo de lotería" si no es ninguna de estas
### imprime "Puedes ir de truco o trato"


# Todo esto según el desafío
edad = int(input("¿Qué edad tienes?: "))
if(edad >= 18):
    print("Puedes ir a votar")
elif(edad == 17):
    print("Puedes aprender a conducir")
elif(edad == 16):
    print("Puedes comprar un décimo de lotería")
else:
    print("Puedes ir de truco o trato")