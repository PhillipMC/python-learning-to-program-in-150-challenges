### Dibuja los numeros 1 2 y 3, empezando por debajo del numero uno


import turtle
import random

colores = ["black", "indianred", "ForestGreen", "firebrick" ]
turtle.color(random.choice(colores))

turtle.left(90)
turtle.forward(100)

turtle.penup()
turtle.right(90)
turtle.forward(50)
turtle.pendown()

turtle.forward(100)
turtle.right(90)
turtle.forward(50)
turtle.right(90)
turtle.forward(100)
turtle.left(90)
turtle.forward(50)
turtle.left(90)
turtle.forward(100)

turtle.penup()
turtle.forward(50)
turtle.pendown()

turtle.forward(100)
turtle.left(90)
turtle.forward(50)
turtle.left(90)
turtle.forward(50)
turtle.back(50)
turtle.right(90)
turtle.forward(50)
turtle.left(90)
turtle.forward(100)

turtle.exitonclick()