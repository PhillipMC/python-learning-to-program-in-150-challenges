### Muestra al usuario una linea de texto de un poema y pidele un 
### punto de inicio y un punto de final. Muestra los caracteres entre
### esos dos puntos


poema = """Cuando el mar sea redondo
y el sol deje de brillar,
ese será el día
en que te pueda olvidar."""
print(poema)
inicioPoema = int(input("Dime desde donde quieres que empiece el poema: "))
finPoema = int(input("Dime hasta donde quieres que te muestre el poema: "))
print(poema[inicioPoema:finPoema])