### Pide al usuario que escriba una estrofa de una cancion infantil
### Pide al usuario la primera estrofa de una cancion infantil, muestra la longitud de esta
### pide una posicion inicial y una posicion final y muestra la canción por esos puntos seleccionados

cancion1estrofa = input("Dime una estrofa de una canción infantil: ")
inicioEstrofa = int(input(f"Dime la posición de inicio de la estrofa"
                    + f"(en total ocupa la cancion {str(len(cancion1estrofa))}):"))
finEstrofa = int(input("Dime la posición final de la estrofa: "))
print(cancion1estrofa[inicioEstrofa:finEstrofa])