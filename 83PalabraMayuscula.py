### Pide al usuario que escriba una palabra en mayúsculas. Si ellos escriben
### una palabra en minúsculas, pídeles que lo intenten de nuevo. Haz que siga
### así hasta que escriban una palabra completamente en mayúsculas


palabraMayuscula = input("Escribe una palabra en mayúsculas: ")
while not palabraMayuscula.isupper():
    palabraMayuscula = input("Escribe una palabra toda en mayúsculas: ")
print(palabraMayuscula)