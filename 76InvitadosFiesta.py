### Pide al usuario que introduzca los nombres de 3 personas que ellos quieran invitar
### a una fiesta y guardalos en una lista. Después que ellos han introducido los 3 nombres,
### pídeles si ellos quieren añadir otro. Si ellos lo hacen permiteles añadir mas nombres hasta
### que ellos digan no digan "si", cuando esto suceda muestra cuanta gente ellos han invitado a la fiesta


print("Escribe el nombre de 3 personas que quieras invitar a la fiesta")
invitadosFiesta = []
for i in range(3):
    invitadosFiesta.append(input("Dime el nombre de la persona " + str(i + 1) +": "))
masInvitados = input("¿Quieres añadir a más personas?(Si/No): ".lower())
while masInvitados == "si":
    invitadosFiesta.append(input("Dime el nombre de la persona que quieres añadir: "))
    masInvitados = input("¿Quieres añadir a más personas?(Si/No): ".lower())
print(invitadosFiesta)
print("Has invitado a la fiesta a", len(invitadosFiesta), "personas")