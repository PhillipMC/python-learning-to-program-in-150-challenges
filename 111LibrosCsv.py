### Crea un archivo .csv que almacene la información de "libro", "Autor", "Año de publicación"


import csv
info = [{"Libro": "Matar a un Ruiseñor", "Autor": "Harper Lee", "Año Publicación": 1960},
        {"Libro": "Breve historia del tiempo", "Autor": "Stephen Hawking", "Año Publicación": 1988},
        {"Libro": "El Gran Gatsby", "Autor": "F. Scitt Fitzgerald", "Año Publicación": 1922},
        {"Libro": "El hombre que confundió a su mujer con un sombrero", "Autor": "Oliver Sacks", "Año Publicación": 1985},
        {"Libro": "Orgullo y Prejuicio", "Autor": "Jane Austen", "Año Publicación": 1813}]

nombreCampos = info[0].keys()
with open('recursos/Libros.csv', 'w', newline='') as archivoCsv:
    
    escritor = csv.DictWriter(archivoCsv, fieldnames=nombreCampos)

    escritor.writeheader()
    for i in info:
        escritor.writerow(i)