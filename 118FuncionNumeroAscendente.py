### Crea una función que pida al usuario introducir un número y guardarlo como la 
### variable "num". Define otra función que use "num" y cuente de 1 a ese número


def defineNumero():
    num = int(input("Dime un numero: "))
    return num

def contarHasta(num):
    for i in range(num):
        print(i+1)

def main():
    numero = defineNumero()
    contarHasta(numero)

main()