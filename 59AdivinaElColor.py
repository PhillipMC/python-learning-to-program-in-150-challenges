### Muestra 5 colores y pregunta al usuario que escoja uno. Si ellos escogen el mismo
### que el programa ha escogido, diles "Bien Hecho" de otro modo muestra una respuesta
### ingeniosa (p.ej.: "La flor [color] seguro es la más bonita del jardín"
### en la cual involucre al color correcto, pídeles que adivinen nuevamente, si ellos
### aún así no aciertan, mantén la misma pista y preguntándoles hasta que el usuario 
### escoja la respuesta correcta y finalmente muestres el mensaje "Bien Hecho"


import random

print("Rosa, Azul, Verde, Naranja, Violeta")
eleccionUsuario = input("Elige uno de los colores: ")
eleccionPrograma = random.choice(["Rosa", "Azul", "Verde", "Naranja", "Violeta"])
while(eleccionPrograma.lower() != eleccionUsuario.lower()):
    print("\nLa flor " + eleccionPrograma + " seguro es la más bonita del jardín\n")
    print("Rosa, Azul, Verde, Naranja, Violeta")
    eleccionUsuario = input("Elige uno de los colores: ")
print("Bien Hecho")