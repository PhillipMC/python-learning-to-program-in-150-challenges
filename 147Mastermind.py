### Realiza una versión del juego Mastermind que sea totalmente jugable por el usuario
# https://en.wikipedia.org/wiki/Mastermind_(board_game)


import tkinter
from tkinter import ttk
import random
import turtle


TURNOS = 5
COLORES = ("blue", "pink", "yellow", "brown", "black", "lightsalmon")
COLORES_CONSOLA = ()

# COLORES ALEATORIOS DE LA PARTIDA
for _ in range(4):
    COLORES_CONSOLA = (*COLORES_CONSOLA, COLORES[random.randint(0,5)])

ventana = tkinter.Tk()
ventana.title("Mastermind")
ventana.geometry("425x425")
ventana.resizable(False, False)

turnos = tkinter.IntVar()
turnos.initialize(TURNOS)

color1 = tkinter.StringVar(ventana)
color2 = tkinter.StringVar(ventana)
color3 = tkinter.StringVar(ventana)
color4 = tkinter.StringVar(ventana)

mostrarResultado = tkinter.Text(ventana, state=tkinter.DISABLED, height=6, width=35)
canvas = tkinter.Canvas(ventana)
ventanaTortuga = turtle.TurtleScreen(canvas)

def ganasPierdes():
    global turnos 
    if(turnos.get() <= 0):
        mostrarResultado.insert(tkinter.END,f"FIN DEL JUEGO PERDISTE\n")
    else:
        seleccionColores = []
        seleccionColores.append(color1.get())
        seleccionColores.append(color2.get())
        seleccionColores.append(color3.get())
        seleccionColores.append(color4.get())
        if(tuple(seleccionColores) == COLORES_CONSOLA):
            posicionarEsferas()
            mostrarResultado.insert(tkinter.END,f"FIN DEL JUEGO GANASTE\n")
        else:
            posicionarEsferas()
            turnos.set(turnos.get() - 1)

def pintarEsferas(tortuga, color, indice):
    tortuga.down()
    tortuga.pencolor("black")
    if(color == COLORES_CONSOLA[indice - 1]):
        tortuga.pencolor("green")
        mostrarResultado.insert(tkinter.END, f"Color {indice}: posicion Correcta\n")
    elif(color not in COLORES_CONSOLA):
        tortuga.pencolor("red")
        mostrarResultado.insert(tkinter.END,f"Color {indice}: no está\n")
    else:
        mostrarResultado.insert(tkinter.END,f"Color {indice}: está pero no en esta posición\n")
    tortuga.fillcolor(color)
    tortuga.begin_fill()
    tortuga.circle(30)
    tortuga.end_fill()
    tortuga.up()

def posicionarEsferas():
    ventanaTortuga.clear()
    tortuga = turtle.RawTurtle(ventanaTortuga)
    tortuga.speed("fastest")
    tortuga.showturtle()
    tortuga.width(3)
    tortuga.hideturtle()
    tortuga.up()
    # CIRCULO 1
    tortuga.goto(tortuga.pos() + (-125,-75))
    pintarEsferas(tortuga, color1.get(), 1)
    # CIRCULO 2
    tortuga.goto(tortuga.pos() + (100,0))
    pintarEsferas(tortuga, color2.get(), 2)
    # CIRCULO 3
    tortuga.goto(tortuga.pos() + (100,0))
    pintarEsferas(tortuga, color3.get(), 3)
    # CIRCULO 4
    tortuga.goto(tortuga.pos() + (100,0))
    pintarEsferas(tortuga, color4.get(), 4)
    

def comprobarResultado():
    mostrarResultado.config(state = tkinter.NORMAL)
    mostrarResultado.delete('1.0', tkinter.END)
    if (color1.get() and color2.get() and  color3.get() and color4.get()) not in COLORES:
        mostrarResultado.insert(tkinter.END, "Elige los colores para poder jugar")
    else:
        ganasPierdes()    
    mostrarResultado.config(state = tkinter.DISABLED)

print(*COLORES_CONSOLA)

turnosVentana = tkinter.Label(ventana, textvariable = turnos).place(relx = 0.96, rely = 0.02,anchor = 'ne')
tkinter.Label(ventana, text = "Turnos:").place(relx = 0.92, rely = 0.02, anchor = 'ne')
canvas.place(relheight = 0.5, relwidth = 1)
tkinter.ttk.OptionMenu(ventana, color1, "color 1", *COLORES).place(x=15, y=250)
tkinter.ttk.OptionMenu(ventana, color2, "color 2", *COLORES).place(x=115, y=250)
tkinter.ttk.OptionMenu(ventana, color3, "color 3", *COLORES).place(x=215, y=250)
tkinter.ttk.OptionMenu(ventana, color4, "color 4", *COLORES).place(x=315, y=250)
tkinter.Button(ventana, text="Comprobar", command=comprobarResultado).place(x=310, y=300)
mostrarResultado.place(x=10, y=300)


ventana.mainloop()