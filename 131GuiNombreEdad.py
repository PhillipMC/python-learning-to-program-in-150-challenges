### Crea un programa que permita al usuario crear un nuevo archivo "".csv"
### Debería preguntar para introducir el nombre y la edad de una persona y
### entonces les permite añadir este al final de la lista que ellos han creado


import tkinter
import csv
from tkinter import messagebox

ventana = tkinter.Tk()
ventana.title("Lista Numeros")
ventana.geometry("400x400")

nombre = tkinter.StringVar()
edad = tkinter.StringVar()

def crearArchivo():
    with open('python_by_example_150-scripts/recursos/Edades.csv', 'w', newline='') as archivoCsvAdjuntar:
        nombreCampos = ["Nombre", "Edad"]
        escritor = csv.DictWriter(archivoCsvAdjuntar, fieldnames=nombreCampos)
        escritor.writeheader()
    messagebox.showinfo("Info","Archivo Nuevo Creado Exitosamente")

def aniadirArchivo():
    with open('python_by_example_150-scripts/recursos/Edades.csv', 'a+', newline='') as archivoCsvAdjuntar:
        
        nombreCampos = ["Nombre", "Edad"]
        escritor = csv.DictWriter(archivoCsvAdjuntar, fieldnames=nombreCampos)
        nombre = texto.get()
        edad = texto2.get()
        texto.delete(0,tkinter.END)
        texto2.delete(0,tkinter.END)
        escritor.writerow({nombreCampos[0]: nombre, nombreCampos[1]: edad})
    messagebox.showinfo("Info","Registro Guardado Exitosamente")
        

texto = tkinter.Entry(ventana)
texto2 = tkinter.Entry(ventana)

 
etiqueta = tkinter.Label(ventana, text = "Escribe el nombre")
etiqueta.config(font =("Courier", 14))
etiqueta2 = tkinter.Label(ventana, text = "Escribe la edad")
etiqueta2.config(font =("Courier", 14))

boton1 = tkinter.Button(ventana, text = "Crear Archivo", command = crearArchivo)
boton2 = tkinter.Button(ventana, text = "Guardar en archivo", command = aniadirArchivo)
 
etiqueta.pack()
texto.pack()
etiqueta2.pack()
texto2.pack()
boton1.pack()
boton2.pack()

ventana.mainloop()