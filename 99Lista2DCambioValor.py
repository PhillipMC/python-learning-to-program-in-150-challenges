### Cambia los programas previos para pedirle al usuario que fila quieren mostrar
### Muestra esa fila, pideles cual columna en esa fila quieren mostrar y muestra 
### el valor. Pideles si quieren cambiar ese valor, si ellos quieren pideles 
### un nuevo valor y cambialo, y luego muestra la fila con el nuevo valor


lista2D = [[2,5,8], [3,7,4], [1,6,9], [4,2,0]]
print(lista2D)
filaUsuario = int(input("Dime una fila que quieras ver de la lista 2D: "))
print("Esta es la fila",lista2D[filaUsuario])
columnaUsuario = int(input("Dime que columna quieres ver de esta fila: "))
print("Este es el valor", lista2D[filaUsuario][columnaUsuario])
cambiar = input("¿Quieres cambiarlo este valor?(Si/No): ")
if(cambiar.lower() == "si"):
    nuevoValor = int(input("Dime que nuevo valor quieres: "))
    lista2D[filaUsuario][columnaUsuario] = nuevoValor
    print("La fila ahora contiene esto:", lista2D[filaUsuario])