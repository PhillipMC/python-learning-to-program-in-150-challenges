### Crea dos arrays uno que contenga 3 números que el usuario introduzca y uno que
### contenga un conjunto de 5 números aleatorios. Únelos en otro array.
### Ordena este último array y muestralo cada número en una línea separada


from array import *
import random

numerosUsuario = array('i',[])
numerosAleatorios = array('i',[])
for i in range(3):
    numerosUsuario.append(int(input("Introduce un numero a almacenar: ")))
for j in range(5):
    numerosAleatorios.append(random.randint(1, 100))
arrayFinal = sorted(numerosUsuario + numerosAleatorios)
for numeroArray in arrayFinal: #print(*arrayFinal, sep="\n")
    print(str(numeroArray))
