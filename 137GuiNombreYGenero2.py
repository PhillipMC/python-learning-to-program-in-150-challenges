### Cambia el programa anterior así cuando un nuevo nombre y género sea añadido
### a la lista de la caja sea también escrito a un archivo de texto.
### Añade otro botón que muestre eel archivo de texto entero en la ventana de la 
### shell en Python.


import tkinter

ventana = tkinter.Tk()
ventana.title("Nombre y Género")
ventana.geometry("400x400")


genero = tkinter.StringVar(ventana)
genero.set("Hombre")


def insertarNombre():
    nombreYGenero = f"{entradaNombre.get()}, {genero.get()}"
    listaNombres.insert(tkinter.END, nombreYGenero)
    with open("python-learning-to-program-in-150-challenges/recursos/Nombres.txt", 'a+') as archivo:
        archivo.write(nombreYGenero + "\n")

def mostrarArchivo():
    with open("python-learning-to-program-in-150-challenges/recursos/Nombres.txt", 'r') as archivo:
        print(archivo.read())

etiqueta = tkinter.Label(text = "Introduce tu Nombre y Género")
listaGeneros = tkinter.OptionMenu(ventana, genero, "Hombre", "Mujer")
entradaNombre = tkinter.Entry(ventana, name = "nombre")
barraNavegacion = tkinter.Scrollbar(orient=tkinter.VERTICAL)

listaNombres = tkinter.Listbox(
    ventana,
    yscrollcommand = barraNavegacion.set,
    font=("Helvetica", 12)
)

barraNavegacion.config(command=listaNombres.yview)


etiqueta.grid(row = 0, column = 1, pady = 20)
listaGeneros.grid(row = 1, column = 0, padx = 20, pady = 20)
entradaNombre.grid(row = 1, column = 1)
tkinter.Button(text= "\u2713", command = insertarNombre).grid(row = 1, column = 2)
listaNombres.grid(row = 2, column = 1)
barraNavegacion.grid(row = 2,column=2)
tkinter.Button(text= "Mostrar Archivo", command = mostrarArchivo).grid(row = 3, column = 1, pady = 10)


ventana.mainloop()