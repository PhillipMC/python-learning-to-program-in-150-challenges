### Crea un programa que muestre una lista desplegable que contenga varios
### colores y un botón. Cuando el usuario seleccione un color de la lista y clique
### el botón debería cambiar el color de fondo de la ventana a ese color. Para un 
### desafío extra, prueba a evitar usar condicionales "if" para realizar esto.


import tkinter

ventana = tkinter.Tk()
ventana.title("Cambiar el fondo por colores")
ventana.geometry("400x400")


def colorFondo():
    ### ELIMINAR
    color = listaColores.selection_get()
    # color = colorSeleccion.get()
    ventana.configure(bg = color)
    etiqueta.configure(bg = color)


colores = ("light gray", "dark orange", "salmon", "DeepSkyBlue2", "bisque2", "cyan",
         "hot pink")


### ELIMINAR
listaColores = tkinter.Listbox(
    ventana,
    height = 6,
    selectmode = tkinter.SINGLE
)

# colorSeleccion = tkinter.StringVar(ventana)
# colorSeleccion.set(colores[0])

# listaColores = tkinter.OptionMenu(ventana, colorSeleccion, *colores)

### ELIMINAR
for indice, color in enumerate(colores):
   listaColores.insert("end", color)
   listaColores.itemconfig(indice,{'fg':color})
listaColores.configure(bg = "black")


etiqueta = tkinter.Label(text = "Lista de Colores")
etiqueta.pack(pady = 20)
listaColores.pack()
botonPregunta = tkinter.Button(ventana, text = "Cambiar Color", command = colorFondo)
botonPregunta.pack(pady = 20)

ventana.mainloop()

### PARA USAR LA VERSION DEL LIBRO DESCOMENTAR Y BORRAR EL CODIGO
### PREVIO COMENTADOS