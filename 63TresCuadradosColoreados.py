### Dibuja 3 cuadrados en la misma fila con un espacio entre ellos
### rellenalos usando 3 colores diferentes


import turtle
import random

colores = ["black", "indianred", "yellow", "ForestGreen", "firebrick" ]


for i in range(0,3):
    turtle.color(random.choice(colores))
    turtle.begin_fill()
    turtle.pendown()
    for i in range(0,4):
        turtle.forward(100)
        turtle.right(90)
    turtle.end_fill()
    turtle.penup()
    turtle.forward(200)
    


turtle.exitonclick()