### Crea una lista vacía llamada "numeros", pide al usuario que introduzca números
### después de que cada número sea introducido, añadelo al final de la lista de los
### números y muestra la lista. Una vez ellos han introducido 3 números, pídeles
### si ellos aún quieren que el último número que ellos han introducido lo quieren guardar.
### Si dicen que no quita el último elemento de la lista y muestra la lista


numeros = []
for i in range(3): 
    numeros.append(int(input("Introduce un numero: ")))
print("Los numeros son", numeros)
numeroFinalBorrar = input("¿Quieres almacenar el último número?(Si/No): ").lower()
if(numeroFinalBorrar == "no"):
    numeros.remove(numeros[-1])
print("Lista de numeros definitiva:", numeros)