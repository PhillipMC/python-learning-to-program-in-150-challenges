### Una pequeña galería de arte está vendiendo trabajos de diferentes artistas
### y quieren mantener un seguimiento de las pinturas usando una BD SQL. Necesitas
### crear un sistema amigable para el usuario para mantener un seguimiento de las obras
### Esto debería realizarse usandose un GUI, con dos tablas en la BD la tabla 
### "Artistas" compuesta de ("Artista_ID", "Nombre", "Direccion", "Ciudad", "Pais",
### "Codigo_Postal") y la tabla "Obras" compuesta de ("Obras_ID", "Artista_ID",
### "Titulo", "Medio", "Precio").
### La galería de arte debe ser capaz de añadir nuevos artistas y obras de arte.
### Una vez una obra ha sido vendida, la información sobre esa obra debería ser
### eliminada de la BD principall almacenada en un archivo de texto separada.
### Los usuarios deberían ser capaz de buscar por artista, medio o precio.


import tkinter
from tkinter import ttk
import os
import sqlite3
import csv
import re
import tkinter.font as font



ventana = tkinter.Tk()
ventana.title("Piezas de Arte")
ventana.geometry("900x500")
ventana.resizable(True, True)

rutaBD = "python_by_example_150-scripts/recursos/arte.bd"
rutaArtistasEliminados = "python_by_example_150-scripts/recursos/artistasEliminados.csv"
rutaObrasEliminadas = "python_by_example_150-scripts/recursos/obrasEliminadas.csv"

columnasArtistas = ("Artista_ID", "Nombre", "Direccion", "Ciudad", "Pais", "Codigo_Postal")
columnasObras = ("Obras_ID", "Artista_ID", "Titulo", "Medio", "Precio")

artistasObrasListBox = tkinter.Listbox(ventana)
artistasEntrada = tkinter.Entry(ventana, width=20, fg="grey", name="artista")
medioEntrada = tkinter.Entry(ventana, width=20, fg="grey", name="medio")
precioEntrada = tkinter.Entry(ventana, width=20, fg="grey", name="precio")

artistasEntrada.insert(0, "Artista...")
medioEntrada.insert(0, "Medio...")
precioEntrada.insert(0, "Precio...")



def creacionBD():
    with sqlite3.connect(rutaBD) as bd:
        cursor = bd.cursor()
        cursor.execute("""CREATE TABLE IF NOT EXISTS Artistas(
            Artista_ID INTEGER PRIMARY KEY,
            Nombre TEXT NOT NULL,
            Direccion Text NOT NULL,
            Ciudad TEXT,
            Pais TEXT NOT NULL,
            Codigo_Postal TEXT NOT NULL);""")
            
        cursor.execute("""CREATE TABLE IF NOT EXISTS Obras(
                Obras_ID INTEGER PRIMARY KEY,
                Artista_ID INTEGER NOT NULL,
                Titulo TEXT NOT NULL,
                Medio TEXT NOT NULL,
                Precio REAL NOT NULL,
                FOREIGN KEY(Artista_ID) REFERENCES Artistas(Artista_ID));""")
        cursor.close()
        bd.commit()
        bd.close



def insertarArtistas():
    with sqlite3.connect(rutaBD) as bd:
        cursor = bd.cursor()
        cursor.execute("""INSERT INTO Artistas(Artista_ID, Nombre, Direccion, Ciudad, Pais, Codigo_Postal)
                    VALUES(1, 'Martin Leighton', '5 Park Place', 'Peterborough', 'Cambridgeshire', 'PE325LP'),
                        (2, 'Eva Czarniecka', '77 Warner Close', 'Chelmsford', 'Essex', 'CM225FT'),
                        (3, 'Roxy Parkin', '90 Hindihead Road', NULL, 'London', 'SE126WM'),
                        (4, 'Nigel Farnworth', '41 Whitby Road', 'Huntly', 'Aberdeenshire', 'AB545PN'),
                        (5, 'Teresa Tanner', '70 Guild Street', NULL, 'London', 'NW71SP');""")
        cursor.close()
        bd.commit()
        bd.close



def insertarObras():
    with sqlite3.connect(rutaBD) as bd:
        cursor = bd.cursor()
        cursor.execute("""INSERT INTO Obras(Obras_ID, Artista_ID, Titulo, Medio, Precio)
                    VALUES(1, 5, 'Woman with black Labrador', 'Oil', 220),
                        (2, 5, 'Bees & Thistles', 'Watercolor', 85),
                        (3, 2, 'A stroll to Westminster', 'Ink', 190),
                        (4, 1, 'African Giant', 'Oil', 800),
                        (5, 3, 'Water Daemon', 'Acrylic', 1700),
                        (6, 4, 'A Seagull', 'Watercolor', 35),
                        (7, 1, 'Three friends', 'Oil', 1800),
                        (8, 2, 'Summer Breeze', 'Acrylic', 1350),
                        (9, 4, 'Mr Hamster', 'Watercolor', 35),
                        (10, 1, 'Pulpit Rock, Dorset', 'Oil', 600),
                        (11, 5, 'Trawler Dungeness Beach', 'Oil', 195),
                        (12, 2, 'Dance in the snow', 'Oil', 250),
                        (13, 4, 'St Tropez port', 'Ink', 45),
                        (14, 3, 'Pirate Assasin', 'Acrylic', 420),
                        (15, 1, 'Morning Walk', 'Oil', 800),
                        (16, 4, 'A baby barn swallow', 'Watercolor', 35),
                        (17, 4, 'The old working mills', 'Ink', 395);""")
        cursor.close()
        bd.commit()
        bd.close



def verArtistasObras(tabla=""):
    if(tabla == ""):
        if(artistasObrasListBox.get(0).count(',') == 5):
            tabla = "Obras"
        else:
            tabla = "Artistas"
    artistasObrasListBox.delete("0", tkinter.END)
    artistaObraListoInsertar = []
    maxAncho = 0
    with sqlite3.connect(rutaBD) as db:
        cursor = db.cursor()
        cursor.execute(f"SELECT * FROM {tabla}")
        for artistaBD in cursor.fetchall():
            queryArtistaObra = ""
            for valor in artistaBD:
                queryArtistaObra += f", {valor}"
                if(maxAncho < len(queryArtistaObra)):
                    maxAncho = len(queryArtistaObra)
            artistaObraListoInsertar.append(queryArtistaObra[1:])
        db.close
        artistasObrasListBox.config(height=len(artistaObraListoInsertar)+1, width = maxAncho-8)
        artistasObrasListBox.insert(tkinter.END, *artistaObraListoInsertar)
    artistasObrasListBox.pack(side=tkinter.LEFT, anchor="ne", pady=10, padx=5)



def eliminar():
    indiceSelecccionListBox = artistasObrasListBox.curselection()[0]
    elementoEliminar = artistasObrasListBox.get(indiceSelecccionListBox)
    with sqlite3.connect(rutaBD) as db:
        cursor = db.cursor()
        if(artistasObrasListBox.get(0).count(',') == 5):
            cursor.execute(f"DELETE FROM 'Artistas' WHERE Artista_ID={elementoEliminar.split(',')[0].lstrip()}")
            db.commit()
            verArtistasObras("Artistas")
            guardarAArchivo(rutaArtistasEliminados, columnasArtistas, elementoEliminar)
        else:
            cursor.execute(f"DELETE FROM 'Obras' WHERE Obras_ID={elementoEliminar.split(',')[0].lstrip()}")
            db.commit()
            verArtistasObras("Obras")
            guardarAArchivo(rutaObrasEliminadas, columnasObras, elementoEliminar)
 


def guardarAArchivo(ruta, campos, elementoGuardar):
    listaElemento = elementoGuardar.split(',')
    diccionarioElementoGuardar = {}
    for indice, cabecera in enumerate(campos):
        diccionarioElementoGuardar.update({cabecera:str(listaElemento[indice]).lstrip()})
    if(not os.path.exists(ruta)):
        with open (ruta, 'w') as archivoCsvEscribir:
            escritor = csv.DictWriter(archivoCsvEscribir, fieldnames=campos)
            escritor.writeheader()
            escritor.writerow(diccionarioElementoGuardar)
            archivoCsvEscribir.close
    else:
        with open (ruta, 'a', newline='\n') as archivoCsvAdjuntar:
            escritor = csv.DictWriter(archivoCsvAdjuntar, fieldnames=campos)
            escritor.writerow(diccionarioElementoGuardar)
            archivoCsvAdjuntar.close



def buscar():
    precio = precioEntrada.get()
    precioVacio = re.search("^.*\.\.\.$", precio)
    medio = medioEntrada.get()
    medioVacio = re.search("^.*\.\.\.$", medio)
    artista = artistasEntrada.get()
    artistaVacio = re.search("^.*\.\.\.$", artista)
    if(precioVacio is None and medioVacio is None and artistaVacio is None):
        busquedaConsulta(f"SELECT Obras_ID, Nombre, Titulo, Medio, Precio FROM Obras NATURAL JOIN Artistas"
                         + f" WHERE Nombre LIKE '{artista}%'"
                         + f" AND Precio > {precio} AND Medio LIKE '{medio}%'")
    elif(precioVacio is None and medioVacio is not None and artistaVacio is not None):
        busquedaConsulta(f"SELECT * FROM Obras WHERE Precio > {precio}")
    elif(precioVacio is not None and medioVacio is None and artistaVacio is not None):
        busquedaConsulta(f"SELECT * FROM Obras WHERE Medio LIKE '{medio}%'")
    elif(precioVacio is not None and medioVacio is not None and artistaVacio is None):
        busquedaConsulta(f"SELECT Obras_ID, Nombre, Titulo, Medio, Precio FROM Obras NATURAL JOIN Artistas"
                         + f" WHERE Nombre LIKE '{artista}%'")
    elif(precioVacio is None and medioVacio is None and artistaVacio is not None):
        busquedaConsulta(f"SELECT * FROM Obras WHERE Medio LIKE '{medio}%' AND Precio > {precio}")
    elif(precioVacio is None and medioVacio is not None and artistaVacio is None):
        busquedaConsulta(f"SELECT Obras_ID, Nombre, Titulo, Medio, Precio FROM Obras NATURAL JOIN Artistas"
                         + f" WHERE Nombre LIKE '{artista}%' AND Precio > {precio}")
    elif(precioVacio is not None and medioVacio is None and artistaVacio is None):
        busquedaConsulta(f"SELECT Obras_ID, Nombre, Titulo, Medio, Precio FROM Obras NATURAL JOIN Artistas"
                         + f" WHERE Nombre LIKE '{artista}%' AND Medio LIKE '{medio}%'")


   
def busquedaConsulta(consulta):
    artistasObrasListBox.delete("0", tkinter.END)
    artistaObraListoInsertar = []
    maxAncho = 0
    with sqlite3.connect(rutaBD) as db:
        cursor = db.cursor()
        cursor.execute(consulta)
        for artistaBD in cursor.fetchall():
            queryArtistaObra = ""
            for valor in artistaBD:
                queryArtistaObra += f", {valor}"
                if(maxAncho < len(queryArtistaObra)):
                    maxAncho = len(queryArtistaObra)
            artistaObraListoInsertar.append(queryArtistaObra[1:])
        db.close
        artistasObrasListBox.config(height=len(artistaObraListoInsertar)+1, width = maxAncho-8)
        artistasObrasListBox.insert(tkinter.END, *artistaObraListoInsertar)
    artistasObrasListBox.pack(side=tkinter.LEFT, anchor="ne", pady=10, padx=5)



def clickRaton(*args):
    if(args[0].widget.get() == f"{str(args[0].widget._name).capitalize()}..." ):
        args[0].widget.delete(0, "end")
  


def dejarRaton(*args):
    if(args[0].widget.get() == f"{str(args[0].widget._name).capitalize()}..." or len(args[0].widget.get()) == 0 ):
        args[0].widget.delete(0, tkinter.END)
        args[0].widget.config(fg="grey")
        args[0].widget.insert(0, f"{str(args[0].widget._name).capitalize()}...")
    else:
        args[0].widget.config(fg="black")
    ventana.focus()



def insertarBD(tabla, listaValores):
    valores = ""
    for valor in listaValores:
        if(valor.replace('.','',1).isdigit()):
            valores += ", " + valor
        else:
            valores += ", " + "'" + valor + "'"
    with sqlite3.connect(rutaBD) as bd:
        cursor = bd.cursor()
        #EVITAMOS LA INSERCION DE LA PRIMERA COMA
        cursor.execute(f"INSERT INTO {tabla} VALUES({valores[2:]})")
    verArtistasObras(tabla)



def registrar(*args):
    camposVacios = False
    camposRellenar = []
    listaElementosEntrada = []
    for elementoEntrada in args:
        if(elementoEntrada.get() == ""):
            print(f"Tienes que rellenar {elementoEntrada._name}")
            camposVacios = True
            camposRellenar.append(f"Tienes que rellenar {elementoEntrada._name}")
        listaElementosEntrada.append(elementoEntrada.get())
    if(camposVacios):
        artistasObrasListBox.delete("0", tkinter.END)
        artistasObrasListBox.insert(tkinter.END, *camposRellenar)
    else:
        if(len(args) == 5):
            insertarBD("Obras", listaElementosEntrada)
        else:
            insertarBD("Artistas", listaElementosEntrada)



def insertarObra():
    obraID = tkinter.Variable(name="Obra ID")
    obrasIDEtiqueta = tkinter.Label(ventana,text="ID Obra")
    obrasIDEtiqueta.pack(anchor="s")
    obrasIDEntrada = tkinter.Entry(ventana, width=20, fg="grey", textvariable=obraID)
    obrasIDEntrada.pack(anchor="s")
    artistaID = tkinter.Variable(name="Artista ID")
    artistaIDEtiqueta = tkinter.Label(ventana,text="ID Artista")
    artistaIDEtiqueta.pack(anchor="s")
    artistaIDEntrada = tkinter.Entry(ventana, width=20, fg="grey", textvariable=artistaID)
    artistaIDEntrada.pack(anchor="s")
    title = tkinter.Variable(name="Titulo")
    titleEtiqueta = tkinter.Label(ventana,text="Titulo")
    titleEtiqueta.pack(anchor="s")
    titleEntrada = tkinter.Entry(ventana, width=20, fg="grey", textvariable=title)
    titleEntrada.pack(anchor="s")
    medio = tkinter.Variable(name="Medio")
    medioEtiqueta = tkinter.Label(ventana,text="Medio")
    medioEtiqueta.pack(anchor="s")
    medioEntrada = tkinter.Entry(ventana, width=20, fg="grey", textvariable=medio)
    medioEntrada.pack(anchor="s")
    precio = tkinter.Variable(name="Precio")
    precioEtiqueta = tkinter.Label(ventana,text="Precio")
    precioEtiqueta.pack(anchor="s")
    precioEntrada = tkinter.Entry(ventana, width=20, fg="grey", textvariable=precio)
    precioEntrada.pack(anchor="s")
    botonGuardar = tkinter.Button(ventana, text = "Registrar", command = lambda: [registrar(obraID, artistaID, title, medio, precio),
        eliminarBotones(obrasIDEtiqueta, obrasIDEntrada, artistaIDEtiqueta, artistaIDEntrada, titleEtiqueta, titleEntrada, 
            medioEtiqueta, medioEntrada, precioEtiqueta, precioEntrada, botonGuardar)])
    botonGuardar.pack(anchor="s")



def eliminarBotones(*args):
    for elemento in args:
        elemento.destroy()



def insertarArtista():
    artistaID = tkinter.Variable(name="Artista ID")
    artistaIDEtiqueta = tkinter.Label(ventana,text="Artista ID")
    artistaIDEtiqueta.pack(anchor="s")
    artistaIDEntrada = tkinter.Entry(ventana, width=20, fg="grey", textvariable=artistaID)
    artistaIDEntrada.pack(anchor="s")
    nombre = tkinter.Variable(name="Nombre")
    nombreEtiqueta = tkinter.Label(ventana,text="Nombre")
    nombreEtiqueta.pack(anchor="s")
    nombreEntrada = tkinter.Entry(ventana, width=20, fg="grey", textvariable=nombre)
    nombreEntrada.pack(anchor="s")
    direccion = tkinter.Variable(name="Direccion")
    direccionEtiqueta = tkinter.Label(ventana,text="Direccion")
    direccionEtiqueta.pack(anchor="s")
    direccionEntrada = tkinter.Entry(ventana, width=20, fg="grey", textvariable=direccion)
    direccionEntrada.pack(anchor="s")
    ciudad = tkinter.Variable(name="Ciudad")
    ciudadEtiqueta = tkinter.Label(ventana,text="Ciudad")
    ciudadEtiqueta.pack(anchor="s")
    ciudadEntrada = tkinter.Entry(ventana, width=20, fg="grey", textvariable=ciudad)
    ciudadEntrada.pack(anchor="s")
    pais = tkinter.Variable(name="Pais")
    paisEtiqueta = tkinter.Label(ventana,text="Pais")
    paisEtiqueta.pack(anchor="s")
    paisEntrada = tkinter.Entry(ventana, width=20, fg="grey", textvariable=pais)
    paisEntrada.pack(anchor="s")
    codigoPostal = tkinter.Variable(name="Código Postal")
    codigoPostalEtiqueta = tkinter.Label(ventana,text="Código Postal")
    codigoPostalEtiqueta.pack(anchor="s")
    codigoPostalEntrada = tkinter.Entry(ventana, width=20, fg="grey", textvariable=codigoPostal)
    codigoPostalEntrada.pack(anchor="s")
    botonGuardar = tkinter.Button(ventana, text = "Registrar", command = lambda: [registrar(artistaID, nombre, direccion, ciudad, pais, codigoPostal),
        eliminarBotones(artistaIDEtiqueta, artistaIDEntrada, nombreEtiqueta, nombreEntrada, direccionEtiqueta, direccionEntrada, 
            ciudadEtiqueta, ciudadEntrada, paisEtiqueta, paisEntrada, codigoPostalEtiqueta, codigoPostalEntrada, botonGuardar)])
    botonGuardar.pack(anchor="s")



if(not os.path.exists(rutaBD)):
    creacionBD()
    insertarArtistas()
    insertarObras()

verArtistasObras()
print()



artistasEntrada.bind("<Button-1>", clickRaton)
artistasEntrada.bind("<Leave>", dejarRaton)
medioEntrada.bind("<Button-1>", clickRaton)
medioEntrada.bind("<Leave>", dejarRaton)
precioEntrada.bind("<Button-1>", clickRaton)
precioEntrada.bind("<Leave>", dejarRaton)



botonBuscar = tkinter.Button(ventana, text = "Artista/Obra", command = verArtistasObras)
botonBuscar.pack(anchor="ne", pady=10, padx = 20)
botonEliminar = tkinter.Button(ventana, text = "Eliminar Registro", command = eliminar)
botonEliminar.pack(side=tkinter.RIGHT, anchor="ne", padx=5)
botonInsertarObra = tkinter.Button(ventana, text = "\u2795 \u270D", font=font.Font(size=20), command = insertarArtista)
botonInsertarObra.pack(side=tkinter.RIGHT, anchor="ne", padx=5)
botonInsertarObra = tkinter.Button(ventana, text = "\u2795 \u270f", font=font.Font(size=20), command = insertarObra)
botonInsertarObra.pack(anchor="e", padx=5)
artistasEntrada.pack(anchor="center",padx=5)
medioEntrada.pack(anchor="center",padx=5)
precioEntrada.pack(anchor="center",padx=5)
botonBuscar = tkinter.Button(ventana, text = "Buscar", command = buscar)
botonBuscar.pack(anchor="center", padx=5, pady=10)



ventana.mainloop()