### Crea un programa que muestre con interfaz gráfica dos cuadros de texto:
### uno que ponga "Introduce el nombre del alumno" y otro que ponga "Introduce la
### nota del alumno", con dos botones uno para añadir y otro para borrar todo.
### Debería guardar la información en una BD SQL llamada "NotasExamenes.db" cuando
### el botón añadir sea pulsado. El botón "Limpiar" debería limpiar la ventana de texto


import sqlite3
import tkinter


ventana = tkinter.Tk()
ventana.title("Alumnos y Notas")
ventana.geometry("500x200")


with sqlite3.connect("python-learning-to-program-in-150-challenges/recursos/NotasExamenes.db") as bd:
        cursor = bd.cursor()
        cursor.execute("""CREATE TABLE IF NOT EXISTS Alumnos(
            Nombre TEXT PRIMARY KEY,
            Nota REAL NOT NULL);""")
        cursor.close()
        bd.commit()


def añadir():
    with sqlite3.connect("python-learning-to-program-in-150-challenges/recursos/NotasExamenes.db") as bd:
        cursor = bd.cursor()
        alumno = entradaAlumno.get()
        nota = entradaNota.get()
        cursor.execute("""INSERT OR IGNORE INTO Alumnos(Nombre, Nota)
                    VALUES(?, ?);""", (alumno, nota))
        cursor.close()
        bd.commit()

def limpiar():
    entradaAlumno.delete("0", tkinter.END)
    entradaNota.delete("0", tkinter.END)


etiquetaAlumno = tkinter.Label(text = "Introduce el nombre del alumno")
entradaAlumno = tkinter.Entry(ventana, name = "nombreAlumno")
etiquetaNota = tkinter.Label(text = "Introduce la nota del alumno")
entradaNota = tkinter.Entry(ventana, name = "notaAlumno")
botonAñadir = tkinter.Button(text = "Añadir", command = añadir)
botonLimpiar = tkinter.Button(text = "Limpiar", command = limpiar)



etiquetaAlumno.grid(row = 0, column = 0, padx = 20, pady = 40)
entradaAlumno.grid(row = 0, column = 2)
etiquetaNota.grid(row = 1, column = 0)
entradaNota.grid(row = 1, column = 2)
botonAñadir.grid(row = 2, column = 0, pady = 10)
botonLimpiar.grid(row = 2, column = 2, pady = 10)


ventana.mainloop()