### Crea una ventana que pregunte al usuario para introducir un número en una
### caja de texto. Cuando ellos cliquen en un botón usará el código 
### "variable.isdigit()" para comprobar que se vea si es un número entero.
### Si es un número entero, añadelo a la lista, de otro modo borra la caja
### de entrada de texto. Añade otro botón que borre la lista


import tkinter

ventana = tkinter.Tk()
ventana.title("Lista Numeros")
ventana.geometry("400x400")

numero = tkinter.StringVar()
listaNumeros = tkinter.Listbox()


def introducirNumero():
    numero = texto.get()
    if(numero.isdecimal()):
        texto.delete(0, texto.index(tkinter.END))
        listaNumeros.insert(tkinter.END,numero)
        listaNumeros.pack()
    else:
        texto.delete(0, texto.index(tkinter.END))

def borrarLista():
    listaNumeros.delete(0, tkinter.END) 
    listaNumeros.pack_forget()
    texto.focus()

 
texto = tkinter.Entry(ventana)
texto.focus()
 
etiqueta = tkinter.Label(ventana, text = "Escribe un numero entero")
etiqueta.config(font =("Courier", 14))

boton1 = tkinter.Button(ventana, text = "Añadir", command = introducirNumero)

boton2 = tkinter.Button(ventana, text = "Resetear", command = borrarLista)
 
etiqueta.pack()
texto.pack()
boton1.pack()
boton2.pack()

ventana.mainloop()