### Aleatoriamente escoge entre "Cara" o "Cruz". Pide al usuario hacer su elección
### si la elección es la misma que la aleatoria, muestra el mensaje "Has ganado", muestra
### el mensaje "Mala suerte" al final dile al usuario que la elección es [Cara/Cruz]


import random

maquinaEleccion = random.choice(["Cara", "Cruz"])
usuarioEleccion = ""
while(usuarioEleccion.lower() != "cara" and usuarioEleccion.lower() != "cruz"):
    usuarioEleccion = input("Elige entre Cara o Cruz (Cara/Cruz): ")
if(maquinaEleccion.lower() == usuarioEleccion.lower()):
    print("Has ganado")
else:
    print("Mala suerte")
print("La elección era:", maquinaEleccion)