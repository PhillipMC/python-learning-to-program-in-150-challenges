### Crea una variable llamada "compnum" y establece el valor a 50
### pide al usuario que introduzca un número. Mientras ellos no adivinen el número
### "compnum" diles si el valor es demasiado bajo o demasiado alto y pideles que lo intenten otra vez
### Si ellos introducen el mismo valor que "compnum" muestra "Bien hecho has acertado, te ha llevado
### [numero_intentos] intentos"

compnum = 50
total = 0
numero = int(input("Adivina el numero: "))
while numero != compnum:
    if(numero < compnum):
        print("Valor muy bajo")
        total += 1
    else:
        print("Valor muy alto")
        total += 1
    numero = int(input("Adivina el numero: "))
print("Bien hecho has acertado, te ha llevado", total+1, "intentos")