### Dibuja un octágono que usa un color diferente aleatoriamente seleccionado
### de una lista de 6 colores para cada linea


import turtle
import random

colores = ["black", "indianred", "ForestGreen", "firebrick", "pink", "aquamarine", "Deeppink3" ]

turtle.penup()
turtle.forward(100)
turtle.right(90)
turtle.pendown()
turtle.pensize(3)

for i in range(0,8):
    turtle.color(random.choice(colores))
    turtle.right(45)
    turtle.forward(100)


turtle.exitonclick()