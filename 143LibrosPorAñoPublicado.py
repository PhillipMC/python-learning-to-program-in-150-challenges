### Usando "LibrosInfo.db" pide al usuario que introduzca un año y muestre todos 
### los libros publicados después de ese año, ordenador por el año que fueron publicados 


import sqlite3

def librosPorAño():
    with sqlite3.connect("python_by_example_150-scripts/recursos/LibrosInfo.db") as bd:
        cursor = bd.cursor()
        añoPublicado = input("Dime a partir de que año quieres ver los libros que se han publicado: ")
        print()
        cursor.execute("""SELECT Titulo, Autor, Fecha_Publicada FROM Libros 
            WHERE Fecha_Publicada > ? ORDER BY Fecha_Publicada""", [añoPublicado])
        print(f"Estos son los libros publicados a partir de {añoPublicado}:")
        for libro in cursor.fetchall():
            print(f"{libro[0]}, {libro[1]} ({libro[2]})")
        cursor.close()

if __name__ == "__main__":
    librosPorAño()
