### Usando la BD "LibrosInfo" del programa anterior, muestra la lista de autores
### y sus fechas de nacimiento. Pide al usuario que itroduzca un lugar de nacimiento
### entonces muestra el titulo, fecha_publicada y nombre del autor para todos los
### libros por autores que nacieron en la localización que ellos seleccionaron


import sqlite3

def listadeAutores():
    with sqlite3.connect("python_by_example_150-scripts/recursos/LibrosInfo.db") as bd:
        cursor = bd.cursor()
        cursor.execute("""SELECT * FROM AUTORES""")
        print("Estos son los autores y su ciudad natal:")
        for autor in cursor.fetchall():
            print(f"{autor[0]} -> {autor[1]}")
        print()
        cursor.close()

def librosPorCiudad():
    with sqlite3.connect("python_by_example_150-scripts/recursos/LibrosInfo.db") as bd:
        cursor = bd.cursor()
        ciudadAutor = input("Dime la ciudad de nacimiento del autor: ")
        print()
        cursor.execute("""SELECT Titulo, Autor, Fecha_Publicada FROM Autores 
            JOIN Libros ON Nombre = Autor
            WHERE Lugar_de_Nacimiento=?""", [ciudadAutor])
        print(f"Estos son los libros donde la ciudad natal del autor es {ciudadAutor}:")
        for libro in cursor.fetchall():
            print(f"{libro[0]}, {libro[1]} ({libro[2]})")
        cursor.close()

if __name__ == "__main__":
    listadeAutores()
    librosPorCiudad()
