### Usando el archivo "Libros.csv", pide al usuario que introduzca un año de
### inicio y otro de fin, muestra todos los libros lanzados entre esos dos años


import csv

fechaInicio = int(input("Dime una fecha de publicación mínima: "))
fechaFin = int(input("Dime una fecha de publicación máxima: "))

with open ("recursos/Libros.csv", 'r') as archivoCsv:
    lector = csv.DictReader(archivoCsv)
    for fila in lector:
        if(fechaInicio < int(fila["Año Publicación"]) < fechaFin):
            print(fila["Libro"] + ":", fila["Año Publicación"], "(" + fila["Autor"] + ")")