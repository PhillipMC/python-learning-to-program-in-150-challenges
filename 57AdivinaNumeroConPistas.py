### Actualiza el programa anterior para que le diga al usuario si el número
### escogido es muy alto o muy bajo

import random

numero = random.randint(1,10)
usuarioNumero = int(input("Adivina un numero entre el 1-10: "))
while(numero != usuarioNumero):
    if(usuarioNumero > numero):
        print("Numero muy alto")
    else:
        print("Numero muy bajo")
    usuarioNumero = int(input("Escoge otro numero entre el 1-10: "))
print("Bien Hecho")