### Crea una lista de 6 asignaturas, muestrala. Pide al usuario cual de ellas
### no le gusta, borra la asignatura que han escogido y muestrala nuevamente


asignaturas = ["Inglés", "Álgebra", "Informática", "Química", "Física", "Historia"]
print(asignaturas)
asignaturaABorrar = input("¿Cual de estas asignaturas es la que menos te gusta?: ").capitalize()
asignaturas.remove(asignaturaABorrar)
print(asignaturas)