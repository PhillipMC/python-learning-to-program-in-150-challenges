### Realiza un cuestionario de matemáticas con 5 preguntas aleatorias que sumen dos números
### por ejemplo, si ellos aciertan se les suma un punto al marcador, al final del cuestionario
### diles cual fue su puntuación

import random

marcador = 0
for i in range(0,5):
    numero1 = random.randint(1,100)
    numero2 = random.randint(1,100)
    print("\nPregunta", str(i+1) + ")")
    respuesta = int(input("¿Cuánto es " + str(numero1) + "+" + str(numero2) + "?: "))
    if(respuesta == (numero1+numero2)):
        marcador += 1
        print("Correcto")
print("Has obtenido:", str(marcador))