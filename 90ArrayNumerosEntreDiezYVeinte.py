### Pide al usuario que introduzca números. Si ellos introducen un número entre
### 10 y 20, guardalo en el array sino muestra el mensaje "Numero fuera de rango"
### una vez los 5 numeros han sido añadidos, muestra el mensaje 
### "Gracias, la Lista de numeros es: " y cada numero en una linea separada


from array import *

numeros = array('i',[])
for i in range(5):
    numero = int(input("Dime un numero comprendido entre 10-20: "))
    if(10 < numero <= 20):
        numeros.append(numero)
    else:
        print("Numero fuera de rango")
print("Gracias, la Lista de numeros es: ")
for numero in numeros: #print(*numeros, sep="\n")
    print(numero)
