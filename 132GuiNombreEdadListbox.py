### Usando el archivo ".csv" que tu creaste para el anterior desafío, crea 
### un programa que permita añadir nombres y edades a la lista y crea un botón 
### que muestre el contenido del archivo ".csv"


import tkinter
import csv
from tkinter import messagebox

ventana = tkinter.Tk()
ventana.title("Lista Numeros")
ventana.geometry("400x400")

nombre = tkinter.StringVar()
edad = tkinter.StringVar()

def crearArchivo():
    with open('python_by_example_150-scripts/recursos/Edades.csv', 'w', newline='') as archivoCsvEscribir:
        nombreCampos = ["Nombre", "Edad"]
        escritor = csv.DictWriter(archivoCsvEscribir, fieldnames=nombreCampos)
        escritor.writeheader()
    
    listaNombres()
    messagebox.showinfo("Info","Archivo Nuevo Creado Exitosamente")


def aniadirArchivo():
    with open('python_by_example_150-scripts/recursos/Edades.csv', 'a+', newline='') as archivoCsvAdjuntar:
        nombreCampos = ["Nombre", "Edad"]
        escritor = csv.DictWriter(archivoCsvAdjuntar, fieldnames=nombreCampos)
        nombre = texto.get()
        edad = texto2.get()
        texto.delete(0, tkinter.END)
        texto2.delete(0, tkinter.END)
        escritor.writerow({nombreCampos[0]: nombre, nombreCampos[1]: edad})
        
    listaNombres()
    messagebox.showinfo("Info","Registro Guardado Exitosamente")


def listaNombres():
    listaNombresWidget.pack_forget()
    with open('python_by_example_150-scripts/recursos/Edades.csv', 'r', newline='') as archivoCsvLeer:
        lector = csv.DictReader(archivoCsvLeer)
        archivoCsv = []
        listaNombresWidget.delete(0, tkinter.END)
        for linea in lector:
            archivoCsv.append(linea)
        for linea in archivoCsv:
            listaNombresWidget.insert(tkinter.END, linea["Nombre"] +': '+ linea["Edad"])
    listaNombresWidget.pack()


texto = tkinter.Entry(ventana)
texto2 = tkinter.Entry(ventana)

listaNombresWidget = tkinter.Listbox()
 
etiqueta = tkinter.Label(ventana, text = "Escribe el nombre")
etiqueta.config(font =("Courier", 14))
etiqueta2 = tkinter.Label(ventana, text = "Escribe la edad")
etiqueta2.config(font =("Courier", 14))

boton1 = tkinter.Button(ventana, text = "Crear Archivo", command = crearArchivo)
boton2 = tkinter.Button(ventana, text = "Guardar en archivo", command = aniadirArchivo)
boton3 = tkinter.Button(ventana, text = "Mostrar Archivo", command = listaNombres)

 
etiqueta.pack()
texto.pack()
etiqueta2.pack()
texto2.pack()
boton1.pack()
boton2.pack()
boton3.pack()

ventana.mainloop()