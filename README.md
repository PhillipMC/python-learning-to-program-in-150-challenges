# Python Learning to Program in 150 Challenges

## Tabla de Cotenido
1. [Descripcion](#descripcion)
2. [Estructura del Libro](#estructura-del-libro)
3. [Licencia](#licencia)

## Descripcion
Ejercicios realizados por mí para completar el libro:  
![Python By Example](cover.jpg)  
__*Python By Example : Learning to Program in 150 Obstacles*__  
Nichola Lacey  
_ISBN:978-1108716833_  
![más información](https://www.amazon.es/Python-Example-Learning-Program-Challenges/dp/1108716830)

## Estructura del Libro
Este Libro se basa en la siguiente estructura de ejercicios para aprender Python, una primera parte donde se dan desafíos y ejemplos para desarrollos sencillos, y una segunda parte dónde son desafíos más elavorados con todo lo que se ha visto en el libro:

#### Parte 1

- (1-11) Lecciones básicas para familiarizarse con Python
- (12-19) If-else
- (20-26) Cadenas
- (27-34) Librería Math
- (35-44) Bucle For
- (45-51) Bucle While
- (52-59) Librería Random
- (60-68) Librería Gráfica Turtle
- (69-79) Tuplas, listas y diccionarios
- (80-87) Manipulación de Cadenas
- (88-95) Arrays
- (96-104) Listas y Diccionarios Bidimensionales
- (105-110) Manejo de archivos
- (111-117) Manipulación archivos csv
- (118-123) Funciones
- (124-132) Librería para interfaz Gráfica Tkinter
- (133-138) Tkinter avanzado
- (139-145) SQLite3

#### Parte 2
- (146) Cifrado César
- (147) Mastermind
- (148) Gestor de contraseñas csv
- (149) Tabla de multiplicar GUI
- (150) Gestión de una galería de arte pequeña

## Licencia
- Licencia [MIT](LICENCE) 