### Crea el siguiente menú:
# "1) Añadir al archivo csv"
# "2) Ver todos los registros"
# "3) Salir"
### Si se introduce e "1", se permite añadir información al archivo "Salarios.csv"
### el cual almacenará el nombre y el salario. Si se selecciona "2" debería mostrar
### todos los registros en el archivo "Salarios.csv". Si se selecciona "3" debería
### parar el programa. Si se selecciona una opción incorrecta se debería ver un mensaje
### de error. El menú debería aparecer siempre hasta que se seleccione la opción "3" y
### finalice el programa


import csv

def menu():
    while(True):
        print("1) Añadir al archivo csv")
        print("2) Ver todos los registros")
        print("3) Salir")
        seleccion = input("Elige un opción: ")
        match seleccion:
            case "1":
                nuevoSalario()
            case "2":
                verRegistros()
            case "3":
                break
            case _:
                print("Elige una opción válida (1-3): ")

def nuevoSalario():
    with open('python_by_example_150-scripts/recursos/Salarios.csv', 'a+', newline='') as archivoCsvAdjuntar,\
     open('python_by_example_150-scripts/recursos/Salarios.csv', 'r', newline='') as archivoCsvLeer:

        nombre = input("Dime el nombre a adjuntar el salario: ")
        salario = round(float(input("Dime el salario a adjuntar: ")), 2)
        nombreCampos = ["Nombre","Salario"]
        lector = csv.DictReader(archivoCsvLeer)
        escritor = csv.DictWriter(archivoCsvAdjuntar, fieldnames=nombreCampos)
        filas = list(lector)
        if(len(filas) == 0):
            escritor.writeheader()
        escritor.writerow({"Nombre": nombre, "Salario":salario})

def verRegistros():
    with open('python_by_example_150-scripts/recursos/Salarios.csv', 'r', newline='') as archivoCsvLeer:
        lector = csv.DictReader(archivoCsvLeer)
        for linea in lector:
            print(linea)

def main():
    menu()

main()