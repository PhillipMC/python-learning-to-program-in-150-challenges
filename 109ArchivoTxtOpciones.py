### Muestra el siguiente menú al usuario:
# 1) Crea un nuevo archivo
# 2) Muestra el archivo
# 3) Añade un nuevo elemento al archivo 
### Pide al usuario que introduzca 1,2 ó 3, si ellos seleccionan otra cosa
### muestra un mensaje de error
### Si escogen 1 pide al usuario que introduzca una asignatura y luego guardala
### en un nuevo archivo llamado "Asignatura.txt" (debería sobreescribir al ya
### existente)
### Si ellos eligen 2, muestra el contenido de "Asignatura.txt"
### Si ellos eligen 3, pide al usuario que introduzca una nueva asignatura y guardala
### al archivo y entonces muestra el contenido entero del archivo
### Corre el programa varias veces para probar las opciones


print("""1) Crea un nuevo archivo
2) Muestra el archivo
3) Añade un nuevo elemento al archivo""")
seleccion = int(input("Realiza una seleccion(1/2/3): "))
if(seleccion not in (1,2,3)):
    print("Error Tienes que elegir 1, 2 ó 3")
else:
    if(seleccion == 1):
        asignatura = input("Dime una asignatura: ")
        archivo = open("recursos/Asignatura.txt", 'w')
        archivo.write(asignatura + '\n')
        archivo.close()
    elif(seleccion == 2):
        archivo = open("recursos/Asignatura.txt", 'r')
        print(archivo.read())
        archivo.close()
    else:
        asignatura = input("Dime una asignatura: ")
        archivo = open("recursos/Asignatura.txt", 'a')
        archivo.write(asignatura + '\n')
        archivo.close()
        archivo = open("recursos/Asignatura.txt", 'r')
        print(archivo.read())
        archivo.close()