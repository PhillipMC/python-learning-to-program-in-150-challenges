### Pide al usuario que introduzca un número. Manten preguntándole
### hasta que se introduzca un valor mayor que 5 y entonces muestra el mensaje
### "El último número que introdujiste fue [número]"

numero = 0
while numero <= 5:
    numero = int(input("Escribe un número: "))
print("El último número que introdujiste fue", numero)