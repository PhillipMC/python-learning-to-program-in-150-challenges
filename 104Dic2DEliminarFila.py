### Después de obtener los 4 nombres, edades y tallas de pie, pide al usuario
### que introduzca el nombre de la persona que ellos quieren eliminar de la lista
### Borra esta fila de la información y muestra las demás filas en líneas separadas


# Para testeo
# diccionarioPersonas = {'Susana': {'Edad': '33', 'Talla': 41.0}, 
#                       'Pedro': {'Edad': '22', 'Talla': 44.0}, 
#                       'Jose': {'Edad': '40', 'Talla': 42.0}, 
#                       'Laura': {'Edad': '26', 'Talla': 38.0}}
diccionarioPersonas = {}
for i in range(4):
    nombre = input("Escribe un nombre: ").title()
    edad = input("Escribe la edad: ")
    tallaZapato = round(float(input("Escribe la talla de zapato: ")), 1)
    diccionarioPersonas.update({nombre:{"Edad":edad, "Talla":tallaZapato}})
print(diccionarioPersonas)
for nombre in diccionarioPersonas:
    print(nombre +", edad:", diccionarioPersonas[nombre]["Edad"])
personaEliminar = input("Dime una persona a eliminar: ").title()
diccionarioPersonas.pop(personaEliminar, None)
for nombre in diccionarioPersonas:
    print(nombre  + ':', "edad:", diccionarioPersonas[nombre]["Edad"] + ',', "Talla:",
     diccionarioPersonas[nombre]["Talla"])