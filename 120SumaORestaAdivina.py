### Crea el siguiente menú y muestralo al usuario:
# "1) Suma"
# "2) Resta "
# "Introduce 1 ó 2: "
### Si ellos introducen un 1, deberían correr una función que generará 2 números
### aleatorios entre 5 y 20, y preguntará al usuario cual es la suma de ambos.
### averiguará la respuesta correcta y enviará la respuesta del usuario y la 
### respuesta correcta.
### Si ellos introducen un 2 como su selección. debería ejecutarse una función que
### generará un número entre 25 y 50 y otro número entre 1 y 25 y preguntará cual es
### la respuesta de la resta de ambos. De esta manera no tendrás que preocuparte sobre 
### respuestas negativas. Retornará la respuesta del usuario y la respuesta correcta.
### Crea otra función que compruebe si la respuestas del usuairo y la respuesta correcta 
### coincide. Si lo hacen muestra "Correcto", sino muestra "Incorrecto la respuesta era"
### y la respuesta correcta.
### Si ellos no seleccionan una opción válida en el menú deberían de mostrarse un mensaje
### de error


import random

def menu():
    print("1) Suma")
    print("2) Resta ")
    eleccion = input("Introduce 1 ó 2: ")
    return eleccion

def seleccion(eleccion):
    if(eleccion == "1" or eleccion == "2"):
        operacion(eleccion)
    else:
        print("Error, has introducido mal la opción")

def generarNumero(minimoRango, maximoRango):
    return random.randint(minimoRango,maximoRango)

def operacion(eleccion):
    respuesta = 0
    if(eleccion == "1"):
        numero1 = generarNumero(5,20)
        numero2 = generarNumero(5,20)
        print(str(numero1) + ' + ' + str(numero2))
        respuesta = int(input("¿Cuánto es?: "))
        comprobacion(respuesta, (numero1 + numero2))

    else:
        numero1 = generarNumero(25,50)
        numero2 = generarNumero(1,25)
        print(str(numero1) + ' - ' + str(numero2))
        respuesta = int(input("¿Cuánto es?: "))
        comprobacion(respuesta, (numero1 - numero2))

    
def comprobacion(numeroUsuario, numeroOperacion):
    if(numeroUsuario == numeroOperacion):
        print("Correcto")
    else:
        print("Incorrecto")
        print("La respuesta era", numeroOperacion)
    
def main():
    eleccion = menu()
    seleccion(eleccion)

main()