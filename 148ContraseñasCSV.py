### Crea un programa que almacene el ID de los usuario y contraseñas de un sistema
### Debería mostrar el siguiente menú: 
# "1) Crear un nuevo usuario"
# "2) Cambiar contraseña"
# "3) Mostrar todos los usuarios"
# "4) Salir"
### Si el usuario selecciona 1, debería preguntar para que introduzca el ID del 
### usuario, debería comprobar si el ID ya está en la lista, si lo está el programa
### debería para que seleccione otro ID. Una vez el usuario introduzca un ID válido
### debería pedir al usuario por una contraseña, la cual debe tener lo siguiente:
# 8 caracteres como mínimo.
# debería incluir letras mayúsculas
# debería incluir letras minúsculas
# debería incluir números
# y debería incluir algún caracter especial.
### Si se selecciona "2" del menú se necesitará introducir un ID, comprobar si el usuario
### existe en la lista, y si está, permitir al usuario cambiar la contraseña y guardar los
### cambios al archivo .csv. Estar seguro que el programa sólo altera la contraseña existente
### y no crea un nuevo registro.
### Si el usuario selecciona "3" muestra todos los IDs pero no las contraseñas
### Si el usuario selecciona "4" debería detenerse el programa.


import csv
import os

nombreCampos = ("Usuario","Contraseña")

def registrarCambiosContraseña(listaContraseñas):
    with open("python_by_example_150-scripts/recursos/contraseñas.csv", 'w') as archivoCsv:
        escritor = csv.DictWriter(archivoCsv, fieldnames=nombreCampos)
        escritor.writeheader()
        escritor.writerows(listaContraseñas)

def mostrarUsuarios():
    with open("python_by_example_150-scripts/recursos/contraseñas.csv", 'r') as archivoCsvLeer:
        lector = csv.DictReader(archivoCsvLeer)
        for indice, linea in enumerate(lector):
            print(f"{indice +1} - {linea['Usuario']}")
        print()

def registrarUsuario(usuario, contraseña):
    with open ("python_by_example_150-scripts/recursos/contraseñas.csv", 'a', newline='\n') as archivoCsvAdjuntar:
        escritor = csv.DictWriter(archivoCsvAdjuntar, fieldnames=nombreCampos)
        escritor.writerow({"Usuario":usuario, "Contraseña":contraseña})


def cambiarContraseña():
    listaContraseñas = []
    with open ("python_by_example_150-scripts/recursos/contraseñas.csv", 'r') as archivoCsv:
        lector = csv.DictReader(archivoCsv)
        for linea in lector:
            listaContraseñas.append(linea)
    usuarioCambio = input("Introduzca el usuario que quiere que se cambie su contraseña: ")
    indiceUsuario = -1
    for indice, diccionario in enumerate(listaContraseñas):
        if(diccionario['Usuario'] == usuarioCambio):
            indiceUsuario = indice
    if(indiceUsuario != -1):
        nuevaContraseña = crearContraseña()
        if(nuevaContraseña != -1):
            listaContraseñas[indiceUsuario]['Contraseña'] = nuevaContraseña
            registrarCambiosContraseña(listaContraseñas)
            print("Contraseña cambiada")
        else:
            print("La contraseña no se ha modificado")
    else:
        print("El usuario que ha introducido no existe")
    

def evaluacionContraseña():
    contraseñaUsuario = input("Introduzca una contraseña para el usuario: ")
    if(len(contraseñaUsuario) < 8):
        print("Contraseña débil: tiene que tener más de 8 caracteres")
        return -1
    minusculas = False
    mayusculas = False
    numeros = False
    caracterEspecial = False
    for letra in contraseñaUsuario:
        if(letra.islower()):
            minusculas = True
        if(letra.isupper()):
            mayusculas = True
        if(letra.isdigit()):
            numeros = True
        if( not letra.isalnum()):
            caracterEspecial = True
    if(not minusculas):
        print("No hay minusculas")
    if(not mayusculas):
        print("No hay mayúsculas")
    if(not numeros):
        print("No hay digitos")
    if(not caracterEspecial):
        print("No hay caracteres especiales")
    if(minusculas and mayusculas and numeros and caracterEspecial):
        return contraseñaUsuario
    else:
        return -1


def crearContraseña():
    print("\nPara crear una contraseña tiene que tener los siguientes requisitos ")
    print("- Al menos 8 caracteres")
    print("- Debe contener letras mayúsculas")
    print("- Debe contener letras minúsculas")
    print("- Debe contener números")
    print("- Y al menos un caracter especial (!,€,$,%,&,<,>,*,@...)")
    contraseñaAceptada = evaluacionContraseña()
    if(contraseñaAceptada == -1):
        cambioContraseña = input("¿Quieres intentar poner otra contraseña (s/n): ?")
        if(cambioContraseña.lower() == "s"):
            crearContraseña()
        else:
            return -1
    else:
        print("buena contraseña")
        return contraseñaAceptada


def crearUsuario():

    usuarioIntroducir = input("Dime el usuario que quieras crear: ")
    with open ("python_by_example_150-scripts/recursos/contraseñas.csv", 'a+') as archivoCsvAdjuntar:
        contraseñasArchivo = "python_by_example_150-scripts/recursos/contraseñas.csv" 
        if(os.stat(contraseñasArchivo).st_size == 0):
            escritor = csv.DictWriter(archivoCsvAdjuntar, fieldnames=nombreCampos)
            escritor.writeheader()
            escritor.writerow("\n")

    existeUsuario = False
    with open("python_by_example_150-scripts/recursos/contraseñas.csv", 'r') as archivoCsvLeer:
        lector = csv.DictReader(archivoCsvLeer)
        for linea in lector:
            if(usuarioIntroducir == linea["Usuario"]):
                print("Ese usuario ya existe por favor introduzca otro usuario")
                print('\n')
                existeUsuario = True
                crearUsuario()

    if(not existeUsuario):
        contraseñaUsuario = crearContraseña()
        if(contraseñaUsuario != 1):
            registrarUsuario(usuarioIntroducir, contraseñaUsuario)
            print('\n')
        

def menu():
    seleccion = ""
    while( seleccion != "4"):
        print("1) Crear un nuevo usuario")
        print("2) Cambiar contraseña")
        print("3) Mostrar todos los usuarios")
        print("4) Salir")
        print()
        seleccion = input("Introduce tu selección: ")
        match seleccion:
            case "1":
                crearUsuario()
            case "2":
                cambiarContraseña()
            case "3":
                mostrarUsuarios()
            case "4":
                print("Adiós")
            case _:
                print("Elige entre 1, 2, 3 ó 4 a la hora de introducir una opción")

if __name__ == "__main__":
    menu()