### Dibuja un cuadrado


import turtle

turtle.begin_fill()
turtle.color("black", "black")

for i in range(0,4):
    turtle.forward(100)
    turtle.right(90)

turtle.end_fill()
turtle.exitonclick()