### Haz un programa que permita al usuario convertir de millas a Kilometros
### teniendo en cuenta que 1Km = 0.6214 millas


import tkinter

ventana = tkinter.Tk()
ventana.title("Conversor Millas KM")
ventana.geometry("400x400")

millas = 0
kms = 0

def borrarLista():
    widgetFocus = ventana.focus_get().winfo_name()
    if(widgetFocus == "kilometros"):
        kms = float(entradaKms.get())
        millas = kms * 0.6214
        entradaMillas.delete(0,tkinter.END)
        entradaMillas.insert(0,millas)
    elif(widgetFocus == "millas"):
        millas = float(entradaMillas.get())
        kms = millas * 1.6093
        entradaKms.delete(0,tkinter.END)
        entradaKms.insert(0,kms)

etiquetaMillas = tkinter.Label(ventana, text = "Millas")
entradaMillas = tkinter.Entry(ventana, name = "millas")
etiquetaKm = tkinter.Label(ventana, text = "Kilometros")
entradaKms = tkinter.Entry(ventana, name = "kilometros" )
botonConversion = tkinter.Button(ventana, text = "Convertir", command = borrarLista)

etiquetaMillas.pack()
entradaMillas.pack()
etiquetaKm.pack()
entradaKms.pack()
botonConversion.pack()

ventana.mainloop()