### Muestra un menú tal que así:
### 1) Area Cuadrado
### 2) Area Triangulo
### Si elige un 1 el usuario, pidele la longitud de uno de sus lados
### e imprime el área del cuadrado
### si elige el 2, pidele la base y la altura del triangulo y calcula
### su área, si elige otra opción haz que se muestre un mensaje de error

print("1) Area Cuadrado")
print("2) Area Triangulo\n")
numero = input("Introduce un numero: ")
if(numero == "1" ):
    lado = float(input("Dame la longitud de uno de los lados: "))
    print("El área del cuadrado es:", lado*lado)
elif(numero == "2"):
    base = float(input("Dame la base del triángulo: "))
    altura = float(input("Dame la altura del triángulo: "))
    print("El área del triángulo es:", (base*altura) / 2)
else:
    print("Opción seleccionada incorrecta")