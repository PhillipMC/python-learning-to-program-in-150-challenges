### Cambia el programa anterior así una vez el usuario ha completado su lista de nombres
### muestra la lista completa y preguntales que escriban uno de los nombres de la lista.
### Muestra la posición de ese nombre en la lista. Pide al usuario si quieren que la persona
### venga a la fiesta. Si ellos responden "no" borra esa entrada de la lista y muestra la lista nuevamente


print("Escribe el nombre de 3 personas que quieras invitar a la fiesta")
invitadosFiesta = []
for i in range(3):
    invitadosFiesta.append(input("Dime el nombre de la persona " + str(i + 1) +": "))
masInvitados = input("¿Quieres añadir a más personas?(Si/No): ".lower())
while masInvitados == "si":
    invitadosFiesta.append(input("Dime el nombre de la persona que quieres añadir: "))
    masInvitados = input("¿Quieres añadir a más personas?(Si/No): ".lower())
print(invitadosFiesta)
print("\nHas invitado a la fiesta a", len(invitadosFiesta), "personas\n")
invitadoIndice = input("Escribe uno de los invitados a la fiesta de la lista de arriba: ")
print("El invitado", invitadoIndice, "es la invitacion numero", invitadosFiesta.index(invitadoIndice)+1, "a la fiesta")
confirmacionInvitado = input("¿Quieres que este invitado asista a la fiesta?(Si/No): ")
if confirmacionInvitado.lower() == "no":
    invitadosFiesta.remove(invitadoIndice)
print("La lista final de invitados es", invitadosFiesta)