### Crea un array el cual almacene una lista de enteros. Genera 5 numeros aleatorios
### y almacenalos en el array. Muestra el array (muestra cada objeto en una linea separada)


from array import *
import random

numeros = array('i',[])
for i in range(5):
    numeros.append(random.randint(1,100))
for numero in numeros: #print(*numeros, sep="\n")
    print(numero)
