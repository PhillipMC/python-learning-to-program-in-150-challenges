### Introduce una lista de 10 colores. Pide al usuario un número de inicio entre 0 y 4
### y un número de fin entre 5 y 9. Muestra la lista para esos colores entre el inicio
### y el fin de la entrada del usuario


colores = ["Azul", "Naranja", "Indigo", "Granate", "Turquesa", "Rosa", "Verde", "Azul", "Negro", "Blanco"]
coloresInicio = int(input("Dime donde quieres que empiece a mostrar la lista de colores (0-4): "))
coloresFin = int(input("Dime donde quieres que finalice la lista de colores (5-9): "))
print(colores[coloresInicio:coloresFin])