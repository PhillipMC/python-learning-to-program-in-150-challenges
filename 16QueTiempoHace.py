### Pregunta al usuario si está lloviendo en minúsculas
### si dice "sí" preguntale si hace viento, si dice "sí"
### otra vez imprime: "Hace demasiado mal tiempo para coger un paraguas"
### sino imprime "Coge un paraguas", si ninguna respuesta es "sí"
### imprime "Que tengas un buen día"

lluvia = input("¿Está lloviendo? Si/No: ")
if(lluvia.lower() == "si" or lluvia.lower() == "sí"):
    viento = input("¿Hace viento? Si/No: ")
    if(viento.lower() == "si" or viento.lower() == "sí"):
        print("Hace demasiado mal tiempo para coger un paraguas")
    else:
        print("Coge un paraguas")
else:
    print("Que tengas buen día")