### Dibuja un triangulo


import turtle

turtle.color("green", "green")
turtle.begin_fill()
turtle.left(60)
    
for i in range(0,3):
    turtle.forward(100)
    turtle.right(120)

turtle.end_fill()    
turtle.exitonclick()