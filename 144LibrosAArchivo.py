### Usando "LibrosInfo.db" pide al usuario que introduzca un año y muestre todos 
### los libros publicados después de ese año, ordenador por el año que fueron publicados
# 9 - El retrato de Dorian Gray - Oscar Wilde - 1890
# 1 - De Profundis - Oscar Wilde - 1905


import sqlite3

def librosAutorArchivo():
    with sqlite3.connect("python_by_example_150-scripts/recursos/LibrosInfo.db") as bd:
        cursor = bd.cursor()
        autorArchivo = input("Dime un autor para volcar los datos en un archivo: ")
        print()
        cursor.execute("""SELECT * FROM Libros 
            WHERE Autor = ? ORDER BY Fecha_Publicada""", [autorArchivo])
        libros = cursor.fetchall()
        if(len(libros) > 0):
            with open(f"python_by_example_150-scripts/recursos/Libros_{autorArchivo}.txt", 'w+') as escribirArchivo:
                escribirArchivo.write(f"Libros del Autor {autorArchivo}\n")
                for libroTupla in libros:
                    for elemento in libroTupla:
                        if(elemento != libroTupla[len(libroTupla) -1]):
                            escribirArchivo.write(str(elemento) + ' - ')
                        else:
                            escribirArchivo.write(str(elemento) + '\n')
        
        cursor.close()

if __name__ == "__main__":
    librosAutorArchivo()
