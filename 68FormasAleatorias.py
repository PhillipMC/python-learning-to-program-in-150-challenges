### Dibuja un patrón que cambie cada vbez que se ejecute el programa
### usa la función random para escoger el número de líneas del patrón
### el ángulo y la longitud de la linea


import turtle
import random

colores = ["black", "indianred", "ForestGreen", "firebrick", "pink", "aquamarine", "Deeppink3" ]
turtle.color(random.choice(colores))

turtle.pensize(3)

lados = random.randint(10,20)
angulo = random.randint(1,365)  # Fig.Geométricas -> angulo = random.choice([30, 45, 60, 90, 120])
tamanio = random.randint(50, 200)

for i in range(0,lados):        
    turtle.right(angulo)
    turtle.forward(tamanio)

turtle.exitonclick()