### Usando el archivo "Libros.csv", pide al usuario cuantos registros quiere
### añadir a la lista y entonces permiteles añadir tantos como quiera. Después
### de que la información haya sido añadida, pide pro un autor y muestra todos
### los libros en la lista por ese autor. Si no hay libros por ese autor en la lista
### muestra un mensaje indicando que no existen libros


import csv

librosNuevos = int(input("¿Cuántos libros nuevos quieres añadir a la colección?: "))
nombreCampos = []

with open ("recursos/Libros.csv", 'r') as archivoCsv:
    nombreCampos = csv.DictReader(archivoCsv).fieldnames

for i in range(librosNuevos):
    with open("recursos/Libros.csv", 'a', newline='') as archivoCsv:
        nuevoLibro = input("Dime un nuevo Libro: ")
        nuevoAutor = input("Dime el autor: ")
        nuevoAnio = input("Dime el año de publicación: ")

        escritor = csv.DictWriter(archivoCsv, fieldnames=nombreCampos)
        escritor.writerow({"Libro":nuevoLibro, "Autor":nuevoAutor, "Año Publicación":nuevoAnio})

autorBuscado = input("Ver libros del autor: ")

with open ("recursos/Libros.csv", 'r') as archivoCsv:
    lector = csv.DictReader(archivoCsv)
    encontrado = False
    for fila in lector:
        if(autorBuscado.lower() == fila["Autor"].lower()):
            print(fila["Libro"] + ":", fila["Año Publicación"])
            encontrado = True
    if(encontrado == False):
        print("No hay libros para este autor")

