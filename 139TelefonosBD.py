### Crea una base de datos con SQLite llamada "GuiaTelefonos" que contenga una
### tabla con información de la siguiente manera (ID, Nombre, Apellido, Telefono)


import sqlite3

with sqlite3.connect("python_by_example_150-scripts/recursos/GuiaTelefonos.db") as db:
    cursor = db.cursor()
    cursor.execute("""CREATE TABLE IF NOT EXISTS nombres(
        ID integer PRIMARY KEY,
        Nombre text NOT NULL,
        Apellido text NOT NULL,
        Telefono integer);""")
    cursor.execute("""INSERT OR IGNORE INTO nombres(ID, Nombre, Apellido, Telefono)
        VALUES("1", "Simon", "Howels", 01223449752)""")
    cursor.execute("""INSERT OR IGNORE INTO nombres(ID, Nombre, Apellido, Telefono)
        VALUES("2", "Karen", "Phillips", 01954295773)""")
    cursor.execute("""INSERT OR IGNORE INTO nombres(ID, Nombre, Apellido, Telefono)
        VALUES("3", "Darren", "Smith", 01583749012)""")
    cursor.execute("""INSERT OR IGNORE INTO nombres(ID, Nombre, Apellido, Telefono)
        VALUES("4", "Anne", "Jones", 01323567322)""")
    cursor.execute("""INSERT OR IGNORE INTO nombres(ID, Nombre, Apellido, Telefono)
        VALUES("5", "Mark", "Smith", 01223855534)""")
    db.commit
    db.close