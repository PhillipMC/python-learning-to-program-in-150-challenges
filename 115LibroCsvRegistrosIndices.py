### Usando el archivo "Libros.csv", muestra la información en el archivo con
### el número de fila de cada uno


import csv

with open ("recursos/Libros.csv", 'r') as archivoCsv:
    lector = csv.DictReader(archivoCsv)
    for indice, fila in enumerate(lector):
        print(str(indice + 1) + ")", fila["Libro"] + ":", fila["Año Publicación"], "(" + fila["Autor"] + ")")