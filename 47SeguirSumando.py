### Pide al usuario introducir un numero y entonces introducir otro número
### Suma estos dos números juntos y entonces pregunta si ellos quieren sumar otro número
### Si ellos introducen "s", preguntales para introducir otro número y mantente sumando
### númeroshasta que ellos no respondan "s". Una vez el bucle pare muestra el total

numero1 = int(input("Escribe un número a sumar: "))
numero2 = int(input("Escribe otro número: "))
total = numero1 + numero2
seguirSumando = input("¿Quieres seguir sumando números? (Si/No): ")
while seguirSumando.lower() == "si" or seguirSumando.lower() == "s": 
    numero = int(input("Escribe otro número que sumaremos: "))
    total += numero
    seguirSumando = input("¿Quieres seguir sumando números? (Si/No): ")
print("El total es", total)