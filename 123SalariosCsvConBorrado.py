### Cambia el programa anterior para permitir borrar un registro del archivo
### csv "Salarios.csv", mostrando un nuevo menú que realice estas opciones:
# "1) Añadir al archivo csv"
# "2) Ver todos los registros"
# "3) Borrar un registro"
# "4) Salir"
# "Elige un opción: "


import csv

nombreCampos = ["Nombre","Salario"]

def menu():
    while(True):
        print("1) Añadir al archivo csv")
        print("2) Ver todos los registros")
        print("3) Borrar un registro")
        print("4) Salir")
        seleccion = input("Elige un opción: ")
        match seleccion:
            case "1":
                nuevoSalario()
            case "2":
                verRegistros()
            case "3":
                borrarRegistro()
            case "4":
                break
            case _:
                print("Elige una opción válida (1-3): ")

def nuevoSalario():
    with open("python_by_example_150-scripts/recursos/Salarios.csv", 'a+', newline='') as archivoCsvAdjuntar,\
     open("python_by_example_150-scripts/recursos/Salarios.csv", 'r', newline='') as archivoCsvLeer:

        nombre = input("Dime el nombre a adjuntar el salario: ")
        salario = round(float(input("Dime el salario a adjuntar: ")), 2)
        lector = csv.DictReader(archivoCsvLeer)
        escritor = csv.DictWriter(archivoCsvAdjuntar, fieldnames=nombreCampos)
        filas = list(lector)
        if(len(filas) == 0):
            escritor.writeheader()
        escritor.writerow({"Nombre": nombre, "Salario":salario})

def verRegistros():
    with open("python_by_example_150-scripts/recursos/Salarios.csv", 'r', newline='') as archivoCsvLeer:
        lector = csv.DictReader(archivoCsvLeer)
        for linea in lector:
            print(linea)


def borrarRegistro():
    listaRegistros = []
    
    with open ("python_by_example_150-scripts/recursos/Salarios.csv", 'r') as archivoCsv:
        lector = csv.DictReader(archivoCsv)
        for linea in lector:
            listaRegistros.append(linea)

    for indice, linea in enumerate(listaRegistros):
        print(str(indice) + ')',linea)
    print('\n')

    lineaBorrar = int(input("¿Qué registro quieres eliminar?: "))
    del listaRegistros[lineaBorrar]
    print('\n')

    with open("python_by_example_150-scripts/recursos/Salarios.csv", 'w', newline='') as archivoCsv:
        
        escritor = csv.DictWriter(archivoCsv, fieldnames=nombreCampos)

        escritor.writeheader()
        escritor.writerows(listaRegistros)
    
    print("Registro Eliminado")

def main():
    menu()

main()