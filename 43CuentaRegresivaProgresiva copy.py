### Pregunta cual dirección el usuario quiere contar (arriba/abajo), si ellos
### seleccionan arriba, entonces preguntales para un número máximo tope y cuenta desde 1
### hasta ese número, si ellos dicen abajo pidele que introduzca un número por debajo de 20
### y muestra una cuenta regresiva desde 20 hasta el número seleccionado, sino escribe ni arriba
### ni abajo muestra el mensaje "No entiendo"

direccion = input("Hacia que direccion contamos (arriba/abajo): ")
if(direccion.lower() == "arriba"):
    numero = int(input("Dime un numero tope: "))
    for i in range(1, numero+1):
        print(i)
elif(direccion.lower() == "abajo"):
    numero = int(input("Dime un número por debajo de 20: "))
    for i in range(20, numero-1, -1):
        print(i)
else:
    print("No entiendo")