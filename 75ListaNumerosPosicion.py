### Crea una lista de 4 números de 3 dígitos, muestra la lista al usuario
### muestra cada valor de la lista en una línea separada. Pide al usuario
### que introduzca un número de 3 dígitos. Si el número que ellos han introducido
### coincide con uno de la lista, muestra la posición de ese número en la lista,
### de otro modo muestra el mensaje "Ese numero no está en la lista"


numeros = [333, 222, 111]
for i in numeros:
    print(i)
numeroUsuario = int(input("Dime un numero de 3 digitos: "))
if numeros.count(numeroUsuario) == 0:
    print("Ese numero no está en la lista")
else:
    print("Esta en la posición", numeros.index(numeroUsuario))